package io;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import main.Config;
import main.ExpEval;
import main.Global;
import main.User;

// Singleton class to access the database
public class DatabaseHandler {

	private static DatabaseHandler instance = null;
	private Connection connection;
	private int maxBatchSize;

	// Remove the public default constructor by creating a private default one
	private DatabaseHandler() {
		maxBatchSize = Config.getDbBatchSize();
		try {
			connection = DriverManager.getConnection(Config.getDbUrl());
			connection.setAutoCommit(false);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}		
	}

	// Get the singleton instance
	public static DatabaseHandler getInstance() {
		if (instance == null) {
			synchronized (DatabaseHandler.class) {
				if (instance == null) {
					instance = new DatabaseHandler();
				}
			}
		}
		return instance;
	}

	// Create the tables in the database to prepare the field for the insertions
	// The tables have been designed to allow databases merges for easy
	// cross simulations results analysis
	public void createTables() {
		try {
			Statement table = connection.createStatement();

			/*
			table.execute(
					"CREATE TABLE configs ("
							+ "id_simu CHAR(64), "
							+ "runs INT, "
							+ "rounds INT, "
							+ "neighbourhood_size INT, "
							+ "dataset_type CHAR(64), "

							+ "min_subs INT, "
							+ "rating_threshold DOUBLE, "
							+ "static_algo_forced BOOLEAN, "
							+ "algo_to_start_with CHAR(64), "
							+ "nb_times_switch_false_items INT, "

							+ "with_detcurralgo BOOLEAN, "
							+ "with_incprevrounds BOOLEAN, "
							+ "with_incsimnodes BOOLEAN, "
							+ "detcurralgo_weighting DOUBLE, "
							+ "incprevrounds_weighting DOUBLE, "

							+ "incsimnodes_weighting DOUBLE, "
							+ "nb_prev_rounds_to_inc INT, "
							+ "cool_off INT, "
							+ "metric_margin DOUBLE, "
							+ "with_static_seed BOOLEAN, "

							+ "with_max_nodes BOOLEAN, "
							+ "max_nodes INT, "
							+ "PRIMARY KEY (id_simu))");
			 */

			table.execute(
					"CREATE TABLE end_stats ("
							+ "id_simu CHAR(64), "
							+ "nb_users INT, "
							+ "avg_items_per_user INT, "
							+ "end_algo_big DOUBLE, "
							+ "end_algo_jaccard DOUBLE, "
							+ "end_algo_overbig DOUBLE, "
							+ "end_algo_overlap DOUBLE, "
							+ "best_algo_big DOUBLE, "
							+ "best_algo_jaccard DOUBLE, "
							+ "best_algo_overbig DOUBLE, "
							+ "best_algo_overlap DOUBLE, "
							+ "PRIMARY KEY (id_simu))");

			table.execute(
					"CREATE TABLE exp_avgs ("
							+ "id_simu CHAR(64), "
							+ "round INT, "
							+ "recall DOUBLE, "
							+ "precision DOUBLE, "
							+ "nodes_switching DOUBLE, "
							+ "nodes_on_best_algos DOUBLE, "
							+ "similarity_degree DOUBLE, "
							+ "PRIMARY KEY (id_simu, round))");


			table.execute(
					"CREATE TABLE users_algo_history ("
							+ "id_simu CHAR(64), "
							+ "username CHAR(64), "
							+ "round INT, "
							+ "algo CHAR(64), "
							+ "on_best_algo BOOLEAN, "
							+ "PRIMARY KEY (id_simu, username, round))");

			connection.commit();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// Inserts the parameters of the config file in the database
	// (number of runs, number of rounds, ...)
	public void insertConfig() {
		try {
			int inc = 1;

			// The table schema can be found in createTables()
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO configs "
							+ "VALUES ("
							+ "?, ?, ?, ?, ?, "
							+ "?, ?, ?, ?, ?, "
							+ "?, ?, ?, ?, ?, "
							+ "?, ?, ?, ?, ?, "
							+ "?, ?)");

			statement.setString(inc++,  Config.getSimulationId());
			statement.setInt(inc++,     Config.getNumRuns());
			statement.setInt(inc++,     Config.getNumRounds());
			statement.setInt(inc++,     Config.getNeighbourhoodSize());
			statement.setString(inc++,  Config.getDatasetType());

			statement.setInt(inc++,     Config.getMinSubscribers());
			statement.setDouble(inc++,  Config.getRatingThreshold());
			statement.setBoolean(inc++, Config.isStaticAlgoForced());
			statement.setString(inc++,  Config.getAlgoToStartWith());
			statement.setInt(inc++,     Config.getNumTimesToSwitchFalseItems());

			statement.setBoolean(inc++, Config.withDetCurrAlgo());
			statement.setBoolean(inc++, Config.withIncPrevRounds());
			statement.setBoolean(inc++, Config.withIncSimNodes());
			statement.setDouble(inc++,  Config.getDetCurrAlgoWeighting());
			statement.setDouble(inc++,  Config.getIncPrevRoundsWeighting());

			statement.setDouble(inc++,  Config.getIncSimNodesWeighting());
			statement.setInt(inc++,     Config.getNumPrevRoundsToInc());
			statement.setInt(inc++,     Config.getRoundsOfCoolOff());
			statement.setDouble(inc++,  Config.getMetricMargin());
			statement.setBoolean(inc++, Config.withStaticSeed());

			statement.setBoolean(inc++, Config.withMaxNodes());
			statement.setInt(inc++,     Config.getMaxNodes());

			statement.execute();
			connection.commit();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// Inserts some statistics computed at the end of the experiment
	public void insertEndStats(List<User> users) {
		// Count the average number of items per user and the algo used
		// by each user at the end of the experiment
		int averageItems = 0;
		double end_big = 0, end_jaccard = 0, end_overlap = 0, end_overbig = 0;
		double best_big = 0, best_jaccard = 0, best_overlap = 0, best_overbig = 0;
		double toPercentage = (double) 100 / Global.getNumNodes();

		for (User user: users) {
			averageItems += user.getVisItems().size() + user.getHiddenItems().size();

			// TODO: use global array algo index to name (see CalcMain/printEndStats())
			//       and in the table creation as well
			String endAlgo = user.getCurrAlgo().getAlgoString();
			if (endAlgo.equals("jaccard")) end_jaccard++;
			else if (endAlgo.equals("big")) end_big++;
			else if (endAlgo.equals("overbig")) end_overbig++;
			else if (endAlgo.equals("overlap")) end_overlap++;

			for (String bestAlgo: user.getBestAlgos()) {
				if (bestAlgo.equals("jaccard")) best_jaccard++;
				else if (bestAlgo.equals("big")) best_big++;
				else if (bestAlgo.equals("overbig")) best_overbig++;
				else if (bestAlgo.equals("overlap")) best_overlap++;
			}
		}
		averageItems /= users.size();

		try {
			int inc = 1;

			// The table schema can be found in createTables()
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO end_stats "
							+ "VALUES (?, ?, ?, ?, ?, "
							+ "?, ?, ?, ?, ?, "
							+ "?)");

			statement.setString(inc++, Config.getSimulationId());
			statement.setInt(inc++, Global.getNumNodes());
			statement.setInt(inc++, averageItems);
			statement.setDouble(inc++, end_big      * toPercentage);
			statement.setDouble(inc++, end_jaccard  * toPercentage);

			statement.setDouble(inc++, end_overbig  * toPercentage);
			statement.setDouble(inc++, end_overlap  * toPercentage);
			statement.setDouble(inc++, best_big     * toPercentage);
			statement.setDouble(inc++, best_jaccard * toPercentage);
			statement.setDouble(inc++, best_overbig * toPercentage);

			statement.setDouble(inc++, best_overlap * toPercentage);
			statement.execute();
			connection.commit();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// Inserts the averages of the experiment's results in the database
	public void insertExpAverages(double[][] averages) {
		try {
			int currentBatchSize = 0;

			int recallIndex = ExpEval.getDataTypeIndex("recall");
			int precisionIndex = ExpEval.getDataTypeIndex("precision");
			int numChangesIndex = ExpEval.getDataTypeIndex("numChanges");
			int numBestAlgosIndex = ExpEval.getDataTypeIndex("numBestAlgos");
			int similarityDegreeIndex = ExpEval.getDataTypeIndex("similarityDegree");

			// The table schema can be found in createTables()
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO exp_avgs VALUES (?, ?, ?, ?, ?, ?, ?)");

			statement.setString(1, Config.getSimulationId());

			for (int round = 0; round < Config.getNumRounds(); round++) {
				statement.setInt(2, round+1);
				statement.setDouble(3, averages[recallIndex][round]);
				statement.setDouble(4, averages[precisionIndex][round]);
				statement.setDouble(5, averages[numChangesIndex][round]);
				statement.setDouble(6, averages[numBestAlgosIndex][round]);
				statement.setDouble(7, averages[similarityDegreeIndex][round]);
				statement.addBatch();

				// Respect the batch size limit
				currentBatchSize++;
				if (currentBatchSize >= maxBatchSize) {
					statement.executeBatch();
					connection.commit();
					statement.clearBatch();
					currentBatchSize = 0;
				}
			}

			// Commit the remaining updates
			if (currentBatchSize != 0) {
				statement.executeBatch();
				connection.commit();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// Inserts the users algorithm history in the database
	public void insertUsersAlgoHistory(List<User> users) {
		try {
			int currentBatchSize = 0;

			// The table schema can be found in createTables()
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO users_algo_history VALUES (?, ?, ?, ?, ?)");

			statement.setString(1, Config.getSimulationId());

			for (User user: users) {
				statement.setString(2, user.getName());

				for (int round = 0; round < Config.getNumRounds(); round++) {
					String currentAlgo = user.getAlgoRecord().get(round);
					statement.setInt(3, round+1);
					statement.setString(4, currentAlgo);
					statement.setBoolean(5, user.getBestAlgos().contains(currentAlgo));
					statement.addBatch();

					// Respect the batch size limit
					currentBatchSize++;
					if (currentBatchSize >= maxBatchSize) {
						statement.executeBatch();
						connection.commit();
						statement.clearBatch();
						currentBatchSize = 0;
					}
				}
			}

			// Commit the remaining updates
			if (currentBatchSize != 0) {
				statement.executeBatch();
				connection.commit();
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	// Commits the last updates and close the connection to the database
	public void closeConnection() {
		if (connection != null) {
			try {
				connection.commit();
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}
}
