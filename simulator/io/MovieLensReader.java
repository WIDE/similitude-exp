package io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import main.Config;
import main.Global;
import main.User;

// DATA SET: MovieLens 100k/1M/10M ratings
//
// Form: userId itemId rating timestamp
// userId, itemId, rating and timestamp are all int
// userId [1-nb users], itemId [1- nb items], rating [1-5], timestamp epoch time
//
// One rating per line, as many lines as there are ratings
// The delimiter is a tab for the 100k dataset and :: for 1M and 10M

public class MovieLensReader {

	public MovieLensReader() {
	}

	public List<User> read(String filename) {
		// One rating per line, we need a hashmap to store the users
		Map<String, Collection<Long>> usersMap = new HashMap<>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(filename));
			String line = br.readLine();

			while ((line = br.readLine()) != null) {
				// Two differents separators need to be detected (tab and ::)
				// This regex matches one or plus spaces and one or plus :
				String regex = "(\\p{Space}+)|(:+)";
				String[] values = line.split(regex);

				if (values.length != 4) {
					br.close();
					throw new RuntimeException("Unexpected format, expected 4 columns");
				}

				// Read the json object into variables
				String userId = values[0];
				long movieId = Long.parseLong(values[1]);
				double rating = Double.parseDouble(values[2]);

				// TODO: change the way we consider ratings?
				// Add the review if high enough rating
				if (rating >= Config.getRatingThreshold()) {
					// If this user not encountered yet, create new user
					if (!usersMap.containsKey(userId)) {
						usersMap.put(userId, new ArrayList<Long>());
					}

					// Add the movie to the user's items
					usersMap.get(userId).add(movieId);
				}

				// Prepare next iteration
				line = br.readLine();
			}

			// Close reader
			br.close();

		} catch (Exception e) {
			throw new RuntimeException("Dataset read failed: ", e);
		}


		// Now create a list of users from the map
		List<User> users = new ArrayList<>();
		int nodeCounter = 0;

		for (Entry<String, Collection<Long>> entry: usersMap.entrySet()) {
			// Only add X nodes if said so in the config file
			if (Config.withMaxNodes()) {
				nodeCounter++;
				if (nodeCounter >= Config.getMaxNodes()) {
					break;
				}
			}

			users.add(new User(entry.getKey(), entry.getValue()));
		}

		// Update the number of nodes in the simulation
		Global.setNumNodes(users.size());

		return users;
	}
}
