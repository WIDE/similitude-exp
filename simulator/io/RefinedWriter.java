package io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import main.User;

import org.json.simple.JSONValue;

public class RefinedWriter {

	public RefinedWriter() {
	}

	// Write the name, visible items, hidden items and best algos of each user
	// in the json file
	public void writeOut(List<User> users, String filename) {
		List<Object> entireJson = new LinkedList<Object>();

		// For each user, puts its name, visible items, hidden items and best
		// algos in an object (Map) which is then added to the entireJson list
		for (User u: users) {
			Map<Object, Object> obj = new LinkedHashMap<>();

			obj.put("name", u.getName());
			obj.put("visibleSubs", u.getVisItems());
			obj.put("hiddenSubs", u.getHiddenItems());
			obj.put("bestAlgos", u.getBestAlgos());

			entireJson.add(obj);
		}

		// Write the produced json to the output file
		try {
			Files.write(Paths.get(filename), JSONValue.toJSONString(entireJson).getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
