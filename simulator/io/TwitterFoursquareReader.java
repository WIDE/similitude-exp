package io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import main.Config;
import main.Global;
import main.User;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

// DATA SET: Foursquare-Twitter
//
// Form: [{"name":"n1","locs":["l1","l2"],"subs":[1,2]}, ...]
// name is a string, locs is a list of string and subs is a list of longs

public class TwitterFoursquareReader {

	public TwitterFoursquareReader() {
	}

	// Returns a list of users read from the given filename
	public List<User> read(String filename) {
		List<User> users = new ArrayList<>();
		String jsonString = null;
		int emptyNodeCounter = 0;
		int nodeCounter = 0;

		// Loads the entire json in a string to make it parsable
		try {
			jsonString = new String(Files.readAllBytes(Paths.get(filename)), StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Parses the json string, it first loads the entire json in one object
		// that we can explore as a list of maps
		//
		// Types in json-simple can be confusing, see the decoding examples
		// and the mapping between json-simple types and java types
		// JSONArray = java.util.List        JSONObject = java.util.Map
		Object entireJsonObj = JSONValue.parse(jsonString);
		JSONArray entireJsonArray = (JSONArray) entireJsonObj;
		for (Object obj: entireJsonArray) {
			JSONObject jsonObj = (JSONObject) obj;

			// Read the json object into variables
			String name = (String) jsonObj.get("name");
			@SuppressWarnings("unchecked")
			List<Long> items = (List<Long>) jsonObj.get("subs");

			// Only add users who have items
			if (items.isEmpty()) {
				emptyNodeCounter++;
			} else {
				// Add the new user
				users.add(new User(name, items));
			}

			// Stop the reading at X users if set so in the config file
			if (Config.withMaxNodes()) {
				nodeCounter++;
				if (nodeCounter >= Config.getMaxNodes()) {
					break;
				}
			}
		}

		if (emptyNodeCounter != 0) {
			System.out.println("Found " + emptyNodeCounter + " empty nodes in the dataset");
		}

		// Update the number of nodes in the simulation
		Global.setNumNodes(users.size());

		return users;
	}
}
