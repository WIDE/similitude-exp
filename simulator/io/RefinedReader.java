package io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import main.Config;
import main.Global;
import main.User;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

// DATA SET: refined
//
// Form: [{"bestAlgos":["a1","a2"],"name":"n1","hiddenSubs":[1,2]},"visibleSubs":[3,4], ...]
// bestAlgos is a list of string, name is a string, hiddenSubs and visibleSubs are lists of longs

public class RefinedReader {

	public RefinedReader() {
	}

	// Returns a list of users read from the given filename
	public List<User> read(String filename) {
		List<User> users = new ArrayList<>();
		String jsonString = null;
		int nodeCounter = 0;

		// Loads the entire json in a string to make it parsable
		try {
			jsonString = new String(Files.readAllBytes(Paths.get(filename)),
					StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Parses the json string, it first loads the entire json in one object
		// that we can explore as a list of maps
		//
		// Types in json-simple can be confusing, see the decoding examples
		// and the mapping between json-simple types and java types
		// JSONArray = java.util.List        JSONObject = java.util.Map
		Object entireJsonObj = JSONValue.parse(jsonString);
		JSONArray entireJsonArray = (JSONArray) entireJsonObj;
		for (Object obj: entireJsonArray) {
			JSONObject jsonObj = (JSONObject) obj;

			// Read the json object into variables
			String name = (String) jsonObj.get("name");
			@SuppressWarnings("unchecked")
			List<Long> hiddenItems = (List<Long>) jsonObj.get("hiddenSubs");
			@SuppressWarnings("unchecked")
			List<Long> visibleItems = (List<Long>) jsonObj.get("visibleSubs");
			@SuppressWarnings("unchecked")
			List<String> bestAlgos = (List<String>) jsonObj.get("bestAlgos");

			// Add the new user
			users.add(new User(name, hiddenItems, visibleItems, bestAlgos));

			// Stop the reading at X users if set so in the config file
			if (Config.withMaxNodes()) {
				nodeCounter++;
				if (nodeCounter >= Config.getMaxNodes()) {
					break;
				}
			}
		}

		// Update the number of nodes in the simulation
		Global.setNumNodes(users.size());

		return users;
	}
}
