package io;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import main.User;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

public class YellJSONReader {

	public YellJSONReader() {
	}

	public List<User> read(String filename) {
		List<User> users = new ArrayList<>();
		Map<String, List<Long>> userHash = new HashMap<>();
		//Business IDs need to be converted to longs to work with sim behaviour
		List<String> knownBusIDs = new ArrayList<>();

		String jsonString = null;

		// Loads the entire json in a string to make it parsable
		try {
			jsonString = new String(Files.readAllBytes(Paths.get(filename)), StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Parses the json string, it first loads the entire json in one object
		// that we can explore as a list of maps
		//
		// Types in json-simple can be confusing, see the decoding examples
		// and the mapping between json-simple types and java types
		// JSONArray = java.util.List        JSONObject = java.util.Map
		Object entireJsonObj = JSONValue.parse(jsonString);
		JSONArray entireJsonArray = (JSONArray) entireJsonObj;
		for (Object obj: entireJsonArray) {
			JSONObject jsonObj = (JSONObject) obj;

			String userID = (String) jsonObj.get("user_id");
			String businessID = (String) jsonObj.get("business_id");

			// Change businessIDs to longs
			if (!knownBusIDs.contains(businessID)) {
				knownBusIDs.add(businessID);
			}
			long adjustedBusinessID = (long) knownBusIDs.indexOf(businessID);

			// Create user if new
			if (!userHash.containsKey(userID)) {
				userHash.put(userID, new ArrayList<Long>());
			}

			// Add the review (if high enough rating)
			userHash.get(userID).add(adjustedBusinessID);
		}

		// Convert users array to users objects
		for (Entry<String, List<Long>> entry: userHash.entrySet()) {
			users.add(new User(entry.getKey(), entry.getValue()));
		}

		return users;
	}
}
