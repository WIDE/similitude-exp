package algorithms;

import java.util.Collection;

import main.User;

// Algorithm: |items(i) intersect items(j)| + |items(j)|
// A.k.a. overlap + big
public class OverBig extends NeighbourAlgorithm {
	public OverBig(User user) {
		super(user);
	}

	@Override
	public String getAlgoString() {
		return "overbig";
	}

	@Override
	public double getAlgosCoeff(Collection<Long> visItems, User neighbour) {
		Collection<Long> neighbourItems = neighbour.getVisItems();
		int coeffCount = 0;

		// TODO: maybe use ordered array?
		// This calculates the overlap
		for (Long i: neighbourItems) {
			if (visItems.contains(i)) {
				coeffCount++;
			}
		}

		// Now add the big bit
		coeffCount += neighbourItems.size();

		return coeffCount;
	}
}
