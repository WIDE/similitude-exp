package algorithms;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import main.Config;
import main.User;
import util.ComparatorCoeffUser;

public abstract class NeighbourAlgorithm implements Algorithm {

	private User user;
	private Map<User, Double> coeffs;

	public NeighbourAlgorithm(User user) {
		this.user = user;
	}

	public void getUserStore() {
		coeffs = user.getAlgoStore(this);
	}

	// This method should also not affect any variables in user when used for free choice.
	@Override
	public Map<User, Double> refineNeighbourList(Collection<Long> visItems) {
		Map<User, Double> refineStore = new HashMap<>(Config.getNeighbourhoodSize());
		List<User> neighbours = user.getNeighbours();

		// If first run, get user's store for this algo.
		if (coeffs == null) {
			getUserStore();
		}

		// Loop neighbours
		for (User currNeighbour: neighbours) {
			// TODO: right use of ==? could potentially save a lot of time
			// First look for saved values for this neighbour-main user relation
			if (visItems == user.getVisItems() && coeffs.containsKey(currNeighbour)) {
				// Only use the hashmap if all users items are included here.
				refineStore.put(currNeighbour, coeffs.get(currNeighbour));
			} else {
				// Get the coefficient to sort by from current algorithm and
				// store in hashmap
				refineStore.put(currNeighbour, getAlgosCoeff(visItems, currNeighbour));
			}
		}
		// Cache all of temp Hashmap on to user to store for future efficiency (Only if all users vis locs in use.)
		if (visItems == user.getVisItems()) {
			coeffs.putAll(refineStore);
		}

		// Put hashmap into tree map, to order it.
		TreeMap<User, Double> orderedCoeffs = new TreeMap<User, Double>(
				new ComparatorCoeffUser(refineStore));
		orderedCoeffs.putAll(refineStore);

		// Add X top entries from the treemap to a new array to return as best neighbours
		Map<User, Double> topNeighbours = new HashMap<>(Config.getNeighbourhoodSize());
		for (int i = 0; i < Config.getNeighbourhoodSize(); i++) {
			Entry<User, Double> topEntry = orderedCoeffs.pollFirstEntry();
			topNeighbours.put(topEntry.getKey(), topEntry.getValue());
		}

		return topNeighbours;
	}
}
