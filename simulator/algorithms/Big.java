package algorithms;

import java.util.Collection;

import main.User;

// Algorithm: |items(j)|
public class Big extends NeighbourAlgorithm {
	public Big(User user) {
		super(user);
	}

	@Override
	public String getAlgoString() {
		return "big";
	}

	@Override
	public double getAlgosCoeff(Collection<Long> visItems, User neighbour) {
		return neighbour.getVisItems().size();
	}
}
