package algorithms;

import java.util.Collection;

import main.User;

// Algorithm: |items(i) intersect items(j)|
public class Overlap extends NeighbourAlgorithm {
	public Overlap(User user) {
		super(user);
	}

	@Override
	public String getAlgoString() {
		return "overlap";
	}

	@Override
	public double getAlgosCoeff(Collection<Long> visItems, User neighbour) {
		Collection<Long> neighbourItems = neighbour.getVisItems();
		int coeffCount = 0;

		// |items(i) intersect items(j)|
		for (Long i: neighbourItems) {
			if (visItems.contains(i)) {
				coeffCount++;
			}
		}

		return coeffCount;
	}
}
