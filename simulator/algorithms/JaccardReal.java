package algorithms;

import java.util.Collection;

import main.User;

// JaccardReal algorithm: |items(i) intersect items(j)| / |items(i) union items(j)|
public class JaccardReal extends NeighbourAlgorithm {
	public JaccardReal(User user) {
		super(user);
	}

	@Override
	public String getAlgoString() {
		return "jaccardreal";
	}

	@Override
	public double getAlgosCoeff(Collection<Long> visItems, User neighbour) {
		Collection<Long> neighbourItems = neighbour.getVisItems();
		int jaccardRealTop = 0;
		int jaccardRealBottom = 0;

		// |items(i) intersect items(j)|
		for (Long i: neighbourItems) {
			if (visItems.contains(i)) {
				jaccardRealTop++;
			}
		}

		// |items(i) union items(j)|
   		jaccardRealBottom = visItems.size() + neighbourItems.size() - jaccardRealTop;

		return (double) jaccardRealTop / jaccardRealBottom;
	}
}
