package algorithms;

import java.util.Collection;
import java.util.Map;

import main.User;

public interface Algorithm {

	public Map<User, Double> refineNeighbourList(Collection<Long> visItems);

	public void getUserStore();

	public double getAlgosCoeff(Collection<Long> visItems, User neighbour);

	public String getAlgoString();
}
