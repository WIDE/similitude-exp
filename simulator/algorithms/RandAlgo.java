package algorithms;

import java.util.Collection;

import main.User;

//Control Algorithm, select completely random users
public class RandAlgo extends NeighbourAlgorithm {
	public RandAlgo(User user) {
		super(user);
	}

	@Override
	public String getAlgoString() {
		return "random";
	}

	@Override
	public double getAlgosCoeff(Collection<Long> visItems, User neighbour) {
		return 1.0;
	}

}
