package algorithms;

import java.util.Collection;

import main.User;

// NOTE: we call it Jaccard, but it is not really. See JaccardReal.java, we
// found a typo in the formula after the paper submission.
// Since it's not really Jaccard, it has been renamed into FrereJacc but for
// backward compatibility of the databases, the name remains Jaccard
// Jaccard algorithm: |items(i) intersect items(j)| / |items(i) + items(j)|
public class Jaccard extends NeighbourAlgorithm {
	public Jaccard(User user) {
		super(user);
	}

	@Override
	public String getAlgoString() {
		return "jaccard";
	}

	@Override
	public double getAlgosCoeff(Collection<Long> visItems, User neighbour) {
		Collection<Long> neighbourItems = neighbour.getVisItems();
		int jaccardTop = 0;
		int jaccardBottom = 0;

		// |items(i) intersect items(j)|
		for (Long i: neighbourItems) {
			if (visItems.contains(i)) {
				jaccardTop++;
			}
		}

		// |items(i) + items(j)|
		jaccardBottom = visItems.size() + neighbourItems.size();

		return (double) jaccardTop / jaccardBottom;
	}
}
