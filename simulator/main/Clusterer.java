package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Clusterer {

	private List<User> users;
	private int numUsers;
	private int rounds;
	private int neighbourhoodSize;
	private Evaluater evaluater;
	private int runNum;
	private Random randomGenerator;

	public Clusterer(List<User> users, Evaluater eval, int runNum) {
		this.users 	= users;
		this.evaluater = eval;
		this.runNum = runNum;
		numUsers 	= users.size();
		rounds 		= Config.getNumRounds();
		neighbourhoodSize = Config.getNeighbourhoodSize();

		// Takes its seed from the global random generator
		randomGenerator = new Random(Global.getGlobalRandomGenerator().nextLong());
	}

	public void cluster() {
		// Build initial random neighbourhood outside of main algorithm
		for (User u: users) {
			int counter = neighbourhoodSize;
			while (counter > 0) {
				// Make sure there is exactly neighbourhoodSize neighbours
				if (u.addNeighbour(getRandomUser(u))) {
					counter--;
				}
			}
		}

		for (int i = 0; i < rounds; i++) {
			int cntr = Global.incRoundsCounter(1);
			System.out.println("-- Round " + cntr);

			for (User u: users) {
				// We can't update neighbours on the fly, it will have cascading effects
				// when adding the neighbours' neighbours to the current neighbours (yo dawg)
				// We need to store the new neighbours elsewhere before the other users
				// in the loop refine their own neighbours
				u.prepareNeighboursUpdate();

				// Try adding a random neighbour only once (won't insert if duplicate)
				u.addNeighbour(getRandomUser(u));

				// Add neighbours of each neighbour to the user's list
				// Avoid looping over the user's neighbour list while modifying it
				List<User> oldNeighbours = new ArrayList<>(u.getNeighbours());
				for (User neighbour: oldNeighbours) {
					for (User neighbour2: neighbour.getNeighbours()) {
						u.addNeighbour(neighbour2);
					}
				}

				// Consider changing algorithm
				u.makeAlgoDecision();

				// Refine neighbours list based on the visible items
				Map<User, Double> neighboursWithCoeff = u.getCurrAlgo().refineNeighbourList(u.getVisItems());
				u.setNeighbours(new ArrayList<>(neighboursWithCoeff.keySet()));

				// Check for changes
				u.checkNeighbourhoodChanges();

				// Store the new neighbours and restore the previous neighbours
				// to not affect the computing of the other users' neighbours
				u.switchWithStoredNeighbours();
			}

			// Restore the stored new neighbours computed this round for each users
			for (User u: users) {
				u.switchWithStoredNeighbours();
			}

			if (Config.withDebugPrinting()) {
				System.out.println(users.get(0).getCurrAlgo().getAlgoString());
			}

			// Call evaluation for recall and precision.
			evaluater.roundEval(runNum, i);
		}
	}

	// getRandomUser returns a random user, which isn't itself
	public User getRandomUser(User u) {
		User toRet;
		// Loop until the random user doesn't match current user
		while (true) {
			toRet = users.get(randomGenerator.nextInt(numUsers));
			if (toRet != u) {
				return toRet;
			}
		}
	}
}
