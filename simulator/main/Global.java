package main;

import java.util.Locale;
import java.util.Random;

public class Global {

	// Available algorithms
	private static final int NUM_ALGOS_AVAILABLE = 4;
	private static final String[] ALGO_INDEX_TO_NAME = {"big", "jaccard", "overbig", "overlap"};

	// Locale.US is used to enforce "." as the decimal separator
	private static final Locale LOCALE = Locale.US;

	// Externally set parameters
	private static int numNodes;        // Num nodes to involve in a simulation.
	private static int averageNumItems; // Average number of items per users

	// Random generator to generate the seeds of the other generators
	private static Random globalRandomGenerator;
	private static final long staticSeedValue = 1L;
	private static boolean isGeneratorInitialised = false;

	// Read the variable name :)
	private static int roundsCounter = 0;

	// Locks for variables synchonization, as many locks as there are variables
	private static Object lock1 = new Object();
	private static Object lock2 = new Object();


	/*
	 * Getters & Setters
	 */

	public static int getNumAlgosAvailable() {
		return NUM_ALGOS_AVAILABLE;
	}

	public static String[] getAlgoIndexToName() {
		return ALGO_INDEX_TO_NAME;
	}

	public static Locale getLocale() {
		return LOCALE;
	}

	public static int getNumNodes() {
		return numNodes;
	}

	// If nodes are removed due to to small datasets
	public static void setNumNodes(int i) {
		Global.numNodes = i;
	}

	// Used as the number of recommendations to make to users
	public static int getAverageNumItems() {
		return averageNumItems;
	}

	// Called after reading the users from the json file
	public static void setAverageNumItems(int averageNumItems) {
		Global.averageNumItems = averageNumItems;
	}

	public static Random getGlobalRandomGenerator() {
		if (!isGeneratorInitialised) {
			synchronized (lock2) {
				// Initialise the generator at the first call of this getter
				// The seed can be static depending on the config parameters
				if (!isGeneratorInitialised) {
					isGeneratorInitialised = true;
					globalRandomGenerator = new Random();
					if (Config.withStaticSeed()) {
						globalRandomGenerator.setSeed(staticSeedValue);
					}
				}
			}
		}
		return globalRandomGenerator;
	}

	public static int incRoundsCounter(int increment) {
		synchronized (lock1) {
			roundsCounter += increment;
			return roundsCounter;
		}
	}
}
