package main;

import calculations.CalcMain;
import calculations.CheckOptimalMain;

public class Main {

	// Contains shared initialisation code between the different mains
	public static void main(String[] args) {
		// Setup the config variables by reading the config class
		if (args.length < 1) {
			System.err.println("Need a config file.");
			System.err.println("java main.Main <config file>");
			System.out.println("Exiting program.");
			System.exit(1);
		}
		Config.init(args[0]);

		// Test the presence of the json and the jdbc libraries in the classpath
		// For calc: json and for simu: json, sqlite
		try {
			Class.forName("org.json.simple.JSONValue");

			if (Config.getDatasetType().equals("refined")) {
				Class.forName(Config.getDbConnector());
			}
		} catch (ClassNotFoundException e) {
			String msg1 = "", msg2 = "";
			if (Config.getDatasetType().equals("refined")) {
				msg1 = "You need to add the json library to the classpath.";
				msg2 = "export CLASSPATH=$CLASSPATH:$PATH_TO_JSON_LIB:$PATH_TO_JDBC_LIB";
			} else {
				msg1 = "You need to add the json and the jdbc libraries to the classpath.";
				msg2 = "export CLASSPATH=$CLASSPATH:$PATH_TO_JSON_LIB";
			}
			System.out.println(msg1);
			System.out.println(msg2);
			System.out.println("Aborting program.");
			System.exit(1);
		}

		// Run the wanted main class
		if (Config.getDatasetType().equals("refined")) {
			SimuMain.simuMain();
		} else if (Config.getDatasetType().equals("checkoptimal")) {
			CheckOptimalMain.checkOptimalMain();
		} else {
			CalcMain.calcMain();
		}
	}
}
