package main;

import io.DatabaseHandler;
import io.RefinedReader;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

// Main class used for launching a simulation
public class SimuMain {
	// List of users, it needs to be a list to be able to pick random users from it
	private static List<User> users, usersAfterRun;

	// Merge the eval arrays into one initially empty eval
	private static Evaluater eval;


	// Main for the simulations
	public static void simuMain() {
		// Check if the dataset is refined
		if (!Config.getDatasetType().equals("refined")) {
			System.err.println("The input dataset must be refined.");
			System.out.println("Aborting program.");
			System.exit(1);
		}

		// Read users in from the json file, fill the users variable
		readUsers();

		// Main loop, run the clusters and the evaluation
		runClustersAndEval();

		// Insert the results of the simulation in the database
		dbOperations();
	}

	// Read users in from the json file, fill the users variable
	private static void readUsers() {
		System.out.println("---- Begin input file loading");

		// Refined dataset reader
		users = new RefinedReader().read(Config.getInputFilename());

		// Print basic informations about the dataset: number of users and
		// average number of items per user
		int counter = 0;
		for (User u: users) {
			counter += u.getVisItems().size() + u.getHiddenItems().size();
		}

		// Compute the average number of items
		int avg = counter/users.size();
		Global.setAverageNumItems(avg);

		System.out.println("Num users read:\t" + users.size());
		System.out.println("Average number of items:\t" + avg);
		System.out.println("---- End of input file loading");
		System.out.println();
	}

	// Run clusters and the evaluation for each algo and each run in several
	// threads, and averages the results
	// Updates the user list with the users resulting from the first run
	private static void runClustersAndEval() {
		// Create an empty evaluater to be filled later by the merges
		eval = new Evaluater();

		// Create the thread pool and set the max threads supported
		ExecutorService pool = Executors.newFixedThreadPool(Config.getThreadpoolSize());

		System.out.println("Num runs:\t"    + Config.getNumRuns());
		System.out.println("Num rounds:\t"  + Config.getNumRounds());
		System.out.println("Num threads:\t" + Config.getThreadpoolSize());
		System.out.println();
		System.out.println("=> Max useful threads:\t" +
				Config.getNumRuns());
		System.out.println("=> Total rounds:\t" +
				(Config.getNumRuns() * Config.getNumRounds()));
		System.out.println();
		System.out.println("---- Starting simulation");

		// Add all the runs at once in the executor, they will be executed when
		// a thread is available
		for (int numRun = 0; numRun < Config.getNumRuns(); numRun++) {
			// Inner class just to be able to launch several methods in the
			// same thread, the initialization is a bit of a hack...
			Thread t = new Thread() {
				private List<User> globalUsers, localUsers;
				private Evaluater globalEval, localEval;
				private int numRun;

				Thread initialize(List<User> globalUsers, Evaluater globalEval,
						int numRun) {
					this.globalUsers = globalUsers;
					this.globalEval = globalEval;
					this.numRun = numRun;
					return this;
				}

				@Override
				public void run() {
					// Duplicate the users and eval to have a thread-safe environment
					localUsers = new ArrayList<>();
					for (User u: globalUsers) {
						localUsers.add(new User(u));
					}
					localEval = new Evaluater(localUsers);

					// Run gossiping to cluster users
					new Clusterer(localUsers, localEval, numRun).cluster();

					// Gather the evaluations in one object
					globalEval.mergeWith(localEval);

					// Update the users so they can be inserted into the database
					if (numRun == 0) {
						usersAfterRun = localUsers;
					}
				};

			}.initialize(users, eval, numRun);

			pool.execute(t);
		}

		// Let the tasks finish and deny future entrance
		pool.shutdown();

		// Wait for all the runs to terminate
		try {
			boolean tasksFinished = pool.awaitTermination(Config.getThreadsTimeout(), TimeUnit.SECONDS);
			if (!tasksFinished) {
				System.out.println("Simulation aborted by timeout.");
				System.out.println("Exiting program.");
				System.exit(1);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("---- End of simulation");
		System.out.println();
	}

	// Create database, create tables and insert the results of the simulation
	// into it
	private static void dbOperations() {
		System.out.println("---- Begin database writing");

		// Get the database singleton
		DatabaseHandler db = DatabaseHandler.getInstance();

		// Create the database tables to prepare for the insertions
		db.createTables();

		// Insert the config parameters into the database (for convenience)
		// db.insertConfig();

		// Insert the experiments averages into the database
		eval.insertExpAvgsIntoDatabase(db);

		// Insert the history of algo used by each user into the database
		db.insertUsersAlgoHistory(usersAfterRun);

		// Insert some statistics, that don't fit anywhere else, into the database
		db.insertEndStats(usersAfterRun);

		// Flush and close the database connection
		db.closeConnection();

		System.out.println("---- End of database writing");
		System.out.println();

		// Small split hack to remove the connector from the url
		System.out.println("Result database: " + Config.getDbUrl().split(":")[2]);
	}
}
