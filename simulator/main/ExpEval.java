package main;

import io.DatabaseHandler;

public class ExpEval {

	private double[][][] resultsStore;
	private static final String[] dataTypes = {"recall", "precision",
		"numChanges", "numBestAlgos", "similarityDegree"};
	private int numRuns;
	private int numRounds;

	public ExpEval() {
		numRuns = Config.getNumRuns();
		numRounds = Config.getNumRounds();
		resultsStore = new double[dataTypes.length][numRuns][numRounds];
	}

	// mergeWith put everything that is in the given parameter inside the
	// result store
	public void mergeWith(ExpEval exp) {
		for (int i = 0; i < exp.resultsStore.length; i++) {
			for (int j = 0; j < exp.resultsStore[i].length; j++) {
				for (int k = 0; k < exp.resultsStore[i][j].length; k++) {
					// If both boxes aren't empty, then there is an overlap and
					// it should not happen
					if (resultsStore[i][j][k] != 0.0 && exp.resultsStore[i][j][k] != 0.0) {
						System.out.println("Problem in ExpEval.java: mergeWith():\t"
								+ " i=" + i + " j=" + j + " k=" + k + "\t"
								+ resultsStore[i][j][k] + "\t" + exp.resultsStore[i][j][k]);
					}
					resultsStore[i][j][k] += exp.resultsStore[i][j][k];
				}
			}
		}
	}

	// Adds a round data to the storage array
	public void addData(int run, int round, double recall, double precision,
			double numSwitches, double numBestAlgos, double similarityDegree) {
		resultsStore[0][run][round] = recall;
		resultsStore[1][run][round] = precision;
		resultsStore[2][run][round] = numSwitches;
		resultsStore[3][run][round] = numBestAlgos;
		resultsStore[4][run][round] = similarityDegree;
	}

	// Method to match dataType Strings to a relative int = for readability
	public static int getDataTypeIndex(String type) {
		int typeInt = -1;
		for (int i = 0, j = dataTypes.length; i < j; i++) {
			if (type.equals(dataTypes[i])) {
				typeInt = i;
			}
		}
		return typeInt;
	}

	// Method calculates averages of multiple runs
	private double[][] calcAverages() {
		double[][] expAvgs = new double[dataTypes.length][numRounds];

		//For each data type
		for (int type = 0; type < dataTypes.length; type++) {
			//For each round
			for (int round = 0; round < numRounds; round++) {
				//Add together data from each run
				double total = 0;
				for (int run = 0; run < numRuns; run++) {
					total += resultsStore[type][run][round];
				}
				//Then divide by #runs and store in results array
				expAvgs[type][round] = total/numRuns;
			}
		}
		//Then return
		return expAvgs;
	}

	// Inserts the calculated averages into the database
	public void insertIntoDatabase(DatabaseHandler db) {
		db.insertExpAverages(calcAverages());
	}
}
