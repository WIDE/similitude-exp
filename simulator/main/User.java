package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import algorithms.Algorithm;
import algorithms.Big;
import algorithms.Jaccard;
import algorithms.OverBig;
import algorithms.Overlap;

public class User {

	private final String NAME;
	private Collection<Long> hiddenItems;
	private Collection<Long> visibleItems;
	private List<User> neighbours;
	private List<User> storedNeighbours;
	private Random randomGenerator;
	private Algorithm currAlgo;
	private List<Algorithm> algoList;
	private Map<Algorithm, Map<User, Double>> coeffStores;
	private List<String> bestAlgos;

	//Dynamic Algorithm Choice Variables
	private int coolOff;
	private AlgorithmChooser algoChooser;
	private boolean switchedThisRound;
	private List<String> algoRecord;

	//Variables for printOuts.
	private boolean debugPrintOuts;
	private Algorithm prevAlgo;
	private User lastNeiRmvd;
	private List<User> lastCheckedNeighbourhood;

	//This constructor for a dataset refined by Calculations.DatasetRefiner.java
	public User(String name, Collection<Long> hiddenItems, Collection<Long> visibleItems, List<String> bestAlgos) {
		// Takes its seed from the global random generator
		this.randomGenerator = new Random(Global.getGlobalRandomGenerator().nextLong());

		// Initialise user-specific variables
		this.NAME        = name;
		this.bestAlgos   = bestAlgos;

		// Initialise the items, if the main class is calcmain then rehide the items
		if (Config.getDatasetType().equals("refined")) {
			this.visibleItems = new ArrayList<>(visibleItems);
			this.hiddenItems  = new ArrayList<>(hiddenItems);
		} else {
			// Assuming datasetType == refinedcalc, rehide the items
			List<Long> items = new ArrayList<>(visibleItems);
			items.addAll(hiddenItems);

			List<Collection<Long>> arrayOfItemsArrays = hideSomeItems(items);
			this.visibleItems = arrayOfItemsArrays.get(0);
			this.hiddenItems = arrayOfItemsArrays.get(1);
		}

		// Initialise generic variables
		resetUser();
	}

	//This constructor for Francois' original dataset.
	public User(String name, Collection<Long> items) {
		// Takes its seed from the global random generator
		this.randomGenerator = new Random(Global.getGlobalRandomGenerator().nextLong());

		// Initialise user-specific variables
		this.NAME = name;
		this.bestAlgos = new ArrayList<>();

		// Hide various items
		List<Collection<Long>> arrayOfItemsArrays = hideSomeItems(items);
		this.visibleItems = arrayOfItemsArrays.get(0);
		this.hiddenItems = arrayOfItemsArrays.get(1);

		// Initialise generic variables
		resetUser();
	}

	// Copy constructor, it only duplicates the main fields of a user: hidden
	// items, visible items and best algos if refined, and items if not refined
	public User(User dolly) {
		// Takes its seed from the global random generator
		this.randomGenerator = new Random(Global.getGlobalRandomGenerator().nextLong());

		// Best algos clone
		List<String> ba = new ArrayList<>();
		for (String item: dolly.bestAlgos) {
			ba.add(new String(item));
		}

		// Visible items clone
		Collection<Long> vi = new ArrayList<>();
		for (Long item: dolly.visibleItems) {
			vi.add(new Long(item));
		}

		// Hidden items clone
		Collection<Long> hi = new ArrayList<>();
		for (Long item: dolly.hiddenItems) {
			hi.add(new Long(item));
		}

		this.NAME = dolly.NAME;
		this.bestAlgos = ba;
		this.visibleItems = vi;
		this.hiddenItems = hi;

		// Initialise generic variables
		resetUser();
	}

	// (Re-)Initialise the class variables
	public void resetUser() {
		// Put every algorithms in the list
		algoList = new ArrayList<>();
		algoList.add(new Big(this));
		algoList.add(new Jaccard(this));
		algoList.add(new OverBig(this));
		algoList.add(new Overlap(this));

		neighbours = new ArrayList<>();
		coeffStores = new HashMap<>();
		algoChooser = new AlgorithmChooser(this);
		lastCheckedNeighbourhood = new ArrayList<>();
		algoRecord = new ArrayList<>();

		currAlgo = null;
		lastNeiRmvd = null;
		prevAlgo = null;
		switchedThisRound = false;
		coolOff = 0;
		debugPrintOuts = Config.withDebugPrinting();

		// Start with the choosen algo described in the config file
		int algoIndex = -1;
		String startAlgoName = Config.getAlgoToStartWith();

		if (startAlgoName.equals("") || startAlgoName.equals("random")) {
			// Random algo
			algoIndex = generateAlgoIndex();
		} else {
			String algoName;
			if (startAlgoName.equals("bestalgo")) {
				// One of the best algos (randomized)
				int randomIndex = randomGenerator.nextInt(bestAlgos.size());
				algoName = bestAlgos.get(randomIndex);
			} else {
				// Not random, not bestalgo, it's one of the homogeneous algos
				algoName = startAlgoName;
			}

			// Find the index that goes with the name
			for (int i = 0; i < Global.getAlgoIndexToName().length; i++) {
				if (algoName.equals(Global.getAlgoIndexToName()[i])) {
					algoIndex = i;
					break;
				}
			}
		}

		algoChange(algoIndex);
	}

	public void makeAlgoDecision() {
		// Skip the chooseAlgo() call if the algo should remain the same, but
		// still add the algo to the algo record
		if (!Config.isStaticAlgoForced()) {
			if (coolOff > 0) {
				coolOff--;
			} else {
				algoChooser.chooseAlgo();
			}
		}

		algoRecord.add(currAlgo.getAlgoString());
	}

	//Ensures hashmap is available whenever algo is changed.
	public void algoChange(int index) {
		algoChange(algoList.get(index));
	}

	//Change by passing an Algorithm also, from free choice.
	public void algoChange(Algorithm newAlgo) {
		prevAlgo = currAlgo;
		currAlgo = newAlgo;
		genericAlgoChange(currAlgo);
	}

	//Generic algo change code, to avoid duplication
	public void genericAlgoChange(Algorithm newAlgo) {
		if (!coeffStores.containsKey(newAlgo)) {
			for (Algorithm algo: algoList) {
				coeffStores.put(algo, new HashMap<User, Double>());
			}
		}
		if (debugPrintOuts) printChange(0);
		coolOff = Config.getRoundsOfCoolOff();				//Reset the cool-off period
		switchedThisRound = true;							//Set this to true, for evaluator stats.
	}

	// Method to generate user algorithms - currently assigns a random one each time.
	public int generateAlgoIndex() {
		return randomGenerator.nextInt(Global.getNumAlgosAvailable());
	}

	// Call this method before adding new neighbours
	public void prepareNeighboursUpdate() {
		storedNeighbours = new ArrayList<>(neighbours);
	}

	public void switchWithStoredNeighbours(){
		List<User> tmp = neighbours;
		neighbours = storedNeighbours;
		storedNeighbours = tmp;
	}

	// Returns false if the given user is already in the neighbours
	public boolean addNeighbour(User neighbour) {
		// Avoid duplicates
		if (this == neighbour || neighbours.contains(neighbour)) {
			return false;
		}
		neighbours.add(neighbour);
		if (debugPrintOuts) printChange(1);
		return true;
	}

	//Remove a neighbour
	public void removeNeighbour(User neighbourToBeRemoved) {
		neighbours.remove(neighbourToBeRemoved);
		lastNeiRmvd = neighbourToBeRemoved;
		if (debugPrintOuts) printChange(2);
	}

	public void checkNeighbourhoodChanges() {
		if (debugPrintOuts && !lastCheckedNeighbourhood.equals(neighbours)) {
			//Then neighbourhood has changed
			printChange(3);
			lastCheckedNeighbourhood = new ArrayList<>(neighbours);
		}
	}

	//Note all variables in this method except random generator should be local
	// to the method. (For Free Choice)
	public List<Collection<Long>> hideSomeItems(Collection<Long> items) {
		Collection<Long> visibleItems = new HashSet<>();
		Collection<Long> hiddenItems = new HashSet<>();
		int itemsSize = items.size();

		// Make sure there's always at least 1 hidden - for algo choice
		int numHiddenItems = (int) Math.floor(itemsSize * Config.getFractionHiddenItems());
		if (numHiddenItems == 0) {
			numHiddenItems = 1;
		}

		// Get the indexes of X% of items as the hidden ones
		Set<Integer> hiddenItemsIndex = new HashSet<>();
		int i = 0;
		while (i < numHiddenItems) {
			int index = randomGenerator.nextInt(itemsSize);

			if (!hiddenItemsIndex.contains(index)) {
				hiddenItemsIndex.add(index);
				i++;
			}
		}

		// Depending on the computed random indexes, dispatch the items to the
		// hidden or to the visible collection
		Iterator<Long> it = items.iterator();
		i = 0;
		while (it.hasNext()) {
			Long item = (Long) it.next();

			if (hiddenItemsIndex.contains(i)) {
				hiddenItemsIndex.remove(i);
				hiddenItems.add(item);
			} else {
				visibleItems.add(item);
			}
			i++;
		}

		// Return visible items first, hidden items second.
		List<Collection<Long>> toReturn = new ArrayList<>();
		toReturn.add(visibleItems);
		toReturn.add(hiddenItems);
		return toReturn;
	}

	public void printUserNotes() {
		System.out.println("\nUser name: " + NAME);
		System.out.println("Num of hidden user items: " + hiddenItems.size());
		System.out.println("Num of visible user items: " + visibleItems.size());
		System.out.print("3 Sample (visible) items: ");
		System.out.println("Current neighbourhood: " + neighbours.toString() + "\n");
	}

	public void printChange(int cause) {
		System.out.print("Node " + NAME + " -> ");
		switch (cause) {
		case 0: 		//Change of Algorithm
			System.out.println("Change of Algo: Change from " + prevAlgo.getClass().toString() + " to " + currAlgo.getClass().toString());
			break;
		case 1:			//Adding a neighbour
			System.out.println("Added Neighbour: " + neighbours.get((neighbours.size() - 1)).getName());	//Print out name of last neighbour added.
			break;
		case 2:			//Removing a neighbour
			//System.out.println("Removed Neighbour: " + lastNeiRmvd.getName());
			break;
		case 3: 		//Printing change in neighbourhoods only.
			String addedNeighbours 	= "";							//Builds up strings based on who's added and who's removed.
			String remNeighbours 	= "";
			for (User u: neighbours) {
				if (!lastCheckedNeighbourhood.contains(u)) {
					addedNeighbours = addedNeighbours + ", " + u.getName();
				}
			}
			for (User u: lastCheckedNeighbourhood) {
				if (!neighbours.contains(u)) {
					remNeighbours = remNeighbours + ", " + u.getName();
				}
			}
			System.out.println("Change in Neighbourhood: Added Neighbours: " + addedNeighbours + "\t\tRemoved Neighbours: " + remNeighbours);
			break;
		}
	}


	/*
	 * Getters & Setters
	 */
	public boolean isSwitchedThisRound() {
		return switchedThisRound;
	}

	public void setSwitchedThisRound(boolean switchedThisRound) {
		this.switchedThisRound = switchedThisRound;
	}

	public int getNumNeighbours() {
		return neighbours.size();
	}

	public List<User> getNeighbours() {
		return neighbours;
	}

	// Set neighbours to new array, used when refining neighbours
	public void setNeighbours(List<User> newNeighbours) {
		neighbours = newNeighbours;
	}

	// Wipe all neighbours, used in establishing calculations
	public void clearNeighbours() {
		neighbours.clear();
	}

	// Gets the user's coefficient store for a certain Algorithm.
	public Map<User, Double> getAlgoStore(Algorithm algo) {
		return coeffStores.get(algo);
	}

	public Collection<Long> getVisItems() {
		return visibleItems;
	}

	public Algorithm getCurrAlgo() {
		return currAlgo;
	}

	public List<Algorithm> getAllAlgos() {
		return algoList;
	}

	public String getName() {
		return NAME;
	}

	public boolean hasPrintOuts() {
		return debugPrintOuts;
	}

	public Collection<Long> getHiddenItems() {
		return hiddenItems;
	}

	public void setBestAlgo(List<String> bestAlgos) {
		this.bestAlgos = bestAlgos;
	}

	public List<String> getBestAlgos() {
		return bestAlgos;
	}

	public List<String> getAlgoRecord() {
		return algoRecord;
	}

	public void setHiddenItems(Collection<Long> hiddenItems) {
		this.hiddenItems = hiddenItems;
	}

	public void setVisibleItems(Collection<Long> visibleItems) {
		this.visibleItems = visibleItems;
	}

	// Returns true if node is currently using one of its best algos.
	public boolean hasbestAlgo() {
		return bestAlgos.contains(currAlgo.getAlgoString());
	}
}
