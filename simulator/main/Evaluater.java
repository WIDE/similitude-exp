package main;

import io.DatabaseHandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;

import util.PairCoeffItem;
import algorithms.Algorithm;

public class Evaluater {

	private List<User> users;
	private int numUsers;
	private ExpEval expEval;

	// To protect the object from concurrent access in mergeWith()
	private static Object lock1 = new Object();

	public Evaluater(List<User> users) {
		this.users = users;
		numUsers = users.size();
		expEval = new ExpEval();
	}

	// Constructor for creating an evaluator on the user object - to use createRecs method for free choice.
	// Also used to merge the different Evaluater in the mains
	public Evaluater() {
		expEval = new ExpEval();
	}

	public void roundEval(int runIdx, int roundIdx) {
		double culmRecall = 0;
		double culmPrecision = 0;
		double culmSimilarityDegree = 0;
		double numUsersSwitched = 0;
		double numUsersOnBestAlgo = 0;

		for (User u: users) {
			//Build list of recommendations based on all neighbours minus those already in visible user list
			List<Long> recs = createRecs(u, u.getVisItems(), getCoeffsPerNeighFromUserStore(u, u.getNeighbours(), u.getCurrAlgo()));

			//Then calculate some culmative recall/Precision
			culmRecall += calcRecall(u, recs);
			culmPrecision += calcPrecision(u, recs);
			culmSimilarityDegree += calcSimilarityDegree(u);

			//Count num Users using a 'best algo'
			if (u.hasbestAlgo()) numUsersOnBestAlgo++;

			//Now find how many users have switched Algo's this round
			if (u.isSwitchedThisRound()) numUsersSwitched++;
			u.setSwitchedThisRound(false);
		}

		culmRecall           = culmRecall           / numUsers;
		culmPrecision        = culmPrecision        / numUsers;
		numUsersSwitched     = numUsersSwitched     / numUsers * 100;
		numUsersOnBestAlgo   = numUsersOnBestAlgo   / numUsers * 100;
		culmSimilarityDegree = culmSimilarityDegree / numUsers * 100;

		expEval.addData(runIdx, roundIdx, culmRecall, culmPrecision,
				numUsersSwitched, numUsersOnBestAlgo, culmSimilarityDegree);

		if (Config.withDebugPrinting()) {
			System.out.println(String.format(Global.getLocale(),
					"Average recall:\t\t\t\t\t%.5f", culmRecall));
			System.out.println(String.format(Global.getLocale(),
					"Average precision:\t\t\t\t%.5f", culmPrecision));
			System.out.println(String.format(Global.getLocale(),
					"Percentage of users switching algorithms:\t%.5f", numUsersSwitched));
			System.out.println(String.format(Global.getLocale(),
					"Percentage of users on a best algo:\t\t%.5f", numUsersOnBestAlgo));
			System.out.println(String.format(Global.getLocale(),
					"Similarity degree:\t\t\t\t%.5f", culmSimilarityDegree));
		}
	}

	// Create the recommendations for a user
	// This method doesn't affect any users permenently
	public List<Long> createRecs(User u, Collection<Long> visItems,
			Map<User, Double> neighboursToCoeffs) {
		List<User> neighbours = new ArrayList<>(neighboursToCoeffs.keySet());
		Collection<Long> items = visItems;
		// Initial capacity is set to maximum (to avoid constant resizing)
		Map<Long, Double> itemsToCoeff = new HashMap<>(
				(int) (Config.getNeighbourhoodSize()
						* Global.getAverageNumItems()
						* (1 - Config.getFractionHiddenItems())));

		// Compute the coeff of each item based on its coeff in each neigbour
		for (User n: neighbours) {
			Collection<Long> currNeighbourItems = n.getVisItems();
			double userCoeff = neighboursToCoeffs.get(n);

			for (Long item: currNeighbourItems) {
				// Discard if the user already has this item
				if (items.contains(item)) {
					continue;
				}
				// Add/update the "similarity value"
				if (itemsToCoeff.containsKey(item)) {
					itemsToCoeff.put(item, userCoeff + itemsToCoeff.get(item));
				} else {
					itemsToCoeff.put(item, userCoeff);
				}
			}
		}

		// PQ to order the recs by Coeff
		int maxQueueSize = Global.getAverageNumItems();
		PriorityQueue<PairCoeffItem> queue = new PriorityQueue<>(maxQueueSize);
		Double minCoeff = new Double(-1);

		for (Entry<Long, Double> entry: itemsToCoeff.entrySet()) {
			PairCoeffItem current = new PairCoeffItem(entry.getValue(), entry.getKey());

			// Limited size queue with the smallest coeff first
			if (queue.size() < maxQueueSize) {
				// Queue is not full: add the current element and update the minimum
				queue.add(current);
				if (current.getCoeff() < minCoeff) {
					minCoeff = current.getCoeff();
				}
			} else {
				// Queue is full: add the current element only if relevant
				// considering the minimum
				if (current.getCoeff() > minCoeff) {
					// Remove the minimum
					queue.remove();
					// Add the current element
					queue.add(current);
					// Update the minimum variable
					minCoeff = queue.peek().getCoeff();
				}
			}
		}

		// Convert the priority queue into a list
		List<Long> recs = new ArrayList<>(maxQueueSize);
		while (!queue.isEmpty()) {
			recs.add(queue.poll().getItem());
		}

		return recs;
	}

	// Calculate recall, based on issco.unige.ch
	// Algorithm: Num items correctly found / Num hidden items
	public double calcRecall(User user, List<Long> recs) {
		Collection<Long> hiddenItems = user.getHiddenItems();
		int recallTop = 0;

		for (Long i: hiddenItems) {
			if (recs.contains(i)) {
				recallTop++;
			}
		}
		return (double) recallTop / hiddenItems.size();
	}

	// Calculate the precision, based on issco.unige.ch
	// Algorithm: Num items correctly found / Num items recommended
	public double calcPrecision(User user, List<Long> recs) {
		Collection<Long> hiddenItems = user.getHiddenItems();
		int precisionTop = 0;

		for (Long k: hiddenItems) {
			if (recs.contains(k)) {
				precisionTop++;
			}
		}
		return (double) precisionTop / recs.size();
	}

	// Calculate the similarity degree of the system which is the average of
	// the similarity of each node
	public double calcSimilarityDegree(User user) {
		int similarityDegreeTop = 0;
		String userAlgo = user.getCurrAlgo().getAlgoString();

		// Count the frequencies of the current algo in the neighbourhood
		for (User n: user.getNeighbours()) {
			if (userAlgo.equals(n.getCurrAlgo().getAlgoString())) {
				similarityDegreeTop++;
			}
		}

		return (double) similarityDegreeTop / user.getNeighbours().size();
	}

	// Builds a hashmap of coeffs, for full list of visible items
	public Map<User, Double> getCoeffsPerNeighFromUserStore(User mainUser, List<User> neighbours, Algorithm algo) {
		Map<User, Double> toReturn = new HashMap<>();
		for (User n: neighbours) {
			Double coeff = mainUser.getAlgoStore(algo).get(n);
			toReturn.put(n, coeff);
		}
		return toReturn;
	}

	// Bridge method to insert the averages of the experiment into the database
	public void insertExpAvgsIntoDatabase(DatabaseHandler db) {
		expEval.insertIntoDatabase(db);
	}

	// Merges the current Evaluater object with the one given in argument
	public void mergeWith(Evaluater eval) {
		synchronized (lock1) {
			expEval.mergeWith(eval.expEval);
		}
	}

	// Protected getter for subclasses
	protected List<User> getUsers() {
		return users;
	}
}
