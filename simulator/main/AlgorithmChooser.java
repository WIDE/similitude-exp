package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;

import algorithms.Algorithm;

public class AlgorithmChooser {

	private User user;
	private Collection<Long> userVisItems;
	private List<Algorithm> usersAlgos;

	private Collection<Long> falseHidItems;
	private Collection<Long> falseVisItems;
	private Map<Algorithm, List<Double>> prevRoundScores;

	private Evaluater evaluater;

	private Random randomGenerator;

	public AlgorithmChooser(User user) {
		this.user = user;
		evaluater = new Evaluater();
		userVisItems = user.getVisItems();
		usersAlgos = user.getAllAlgos();

		// Takes its seed from the global random generator
		randomGenerator = new Random(Global.getGlobalRandomGenerator().nextLong());

		// Populate the store for previous rounds scores
		prevRoundScores = new HashMap<>();
		for (Algorithm algo: usersAlgos) {
			prevRoundScores.put(algo, new ArrayList<Double>(Config.getNumPrevRoundsToInc()));
		}
	}

	public void chooseAlgo() {
		Map<Algorithm, Map<User, Double>> algoNeighbourhoods = new HashMap<>();
		Map<Algorithm, List<Long>> algoToRecs;
		Map<Algorithm, Double> algoScores = new HashMap<>();

		// Attempt algo with X sets of false hidden items, to ensure one set does not skew results.
		for (int i = 0; i < Config.getNumTimesToSwitchFalseItems(); i++) {
			generateFalseHiddenItems();

			// Then try each algo with each neighbour in turn
			for (Algorithm algo: usersAlgos) {
				algoNeighbourhoods.put(algo, algo.refineNeighbourList(falseVisItems));
			}

			// Get recommendations from each potential neighbourhood
			algoToRecs = getRecs(algoNeighbourhoods);

			// Get Algos Scores
			algoScores = calcAlgosWeighting(algoToRecs, algoScores);
		}
		// Now divide by the number of times we just switched false items, so we have a base score between 0-1
		for (Algorithm algo: usersAlgos) {
			double score = algoScores.get(algo);
			score = score/Config.getNumTimesToSwitchFalseItems();
			algoScores.put(algo, score);
		}

		/*
		 * Here lie the mighty modifiers!
		 */

		if (Config.withDetCurrAlgo()) {
			algoScores = detCurrAlgo(algoScores, Config.getDetCurrAlgoWeighting());
		}

		if (Config.withIncPrevRounds()) {
			algoScores = incPrevRoundScores(algoScores, Config.getIncPrevRoundsWeighting());
		}

		if (Config.withIncSimNodes()) {
			algoScores = incSimilarNodes(algoScores, algoNeighbourhoods, Config.getIncSimNodesWeighting());
		}

		// Store results in prevRoundStores Hashmap
		for (Algorithm algo: usersAlgos) {
			List<Double> algosRoundScores = prevRoundScores.get(algo);
			algosRoundScores.add(algoScores.get(algo).doubleValue());
		}

		// Pull out the algos with the highest scores into an arraylist
		List<Algorithm> bestAlgos = new ArrayList<>();
		Double maxScore = 0.0;
		for (Algorithm algo: usersAlgos) {
			Double score = algoScores.get(algo);
			if (score > maxScore) {
				bestAlgos.clear();
				bestAlgos.add(algo);
				maxScore = score;
			}
			else if (score == maxScore || score < 0.0005) {
				bestAlgos.add(algo);
			}
		}

		// If current algo is not in list of best ones, change Algo
		if (!bestAlgos.contains(user.getCurrAlgo())) {
			user.algoChange(bestAlgos.get(randomGenerator.nextInt(bestAlgos.size())));		// If more than one are equal, pick randomly.
		}
	}

	// Detriments the score of the metric currently in use
	private Map<Algorithm, Double> detCurrAlgo(
			Map<Algorithm, Double> inputScores,
			double weighting) {
		double score = inputScores.get(user.getCurrAlgo());
		double scoreModifier = score * Config.getDetrimentValue();
		inputScores.put(user.getCurrAlgo(), score - scoreModifier * weighting);
		return inputScores;
	}

	// Incorporate previous round scores
	private Map<Algorithm, Double> incPrevRoundScores(
			Map<Algorithm, Double> inputScores,
			double weighting) {
		int roundNum = -1;

		//Now change the score for each algorithm to include X previous rounds
		for (Algorithm algo: usersAlgos) {
			List<Double> currAlgosHistory = prevRoundScores.get(algo);
			//If first time though, set the round number.
			if (roundNum == -1) {
				roundNum = currAlgosHistory.size();
			}
			//The set score in arraylist to return to include X previous rounds
			Double scoreToRet = 0.0;
			double roundToIncConfig = Config.getNumPrevRoundsToInc();
			for (int i = 0; i < Math.min(roundNum, roundToIncConfig); i++) {
				// For maxRounds = 5, the values are: 0.5, 0.4, 0.3, 0.2, 0.1
				scoreToRet += currAlgosHistory.get(roundNum - i - 1)
						* (Config.getIncPrevRoundsMaxWeight() - i
								* (Config.getIncPrevRoundsMaxWeight()
										/ roundToIncConfig));
			}
			inputScores.put(algo, inputScores.get(algo) + scoreToRet * weighting);
		}

		return inputScores;
	}

	// Incorporate similar nodes
	private Map<Algorithm, Double> incSimilarNodes(
			Map<Algorithm, Double> inputScores,
			Map<Algorithm, Map<User, Double>> algoNeighbourhoods,
			double weighting) {
		// Get the sim score for each neighbour under the current algo in use.
		Set<User> neighbours = algoNeighbourhoods.get(user.getCurrAlgo()).keySet();

		// Calculate the average score between the different metrics
		double avgScore = 0;
		for (Algorithm algo: usersAlgos) {
			avgScore += inputScores.get(algo);
		}
		avgScore /= (double) usersAlgos.size();

		// Count the frequencies of each algo in the neighbourhood
		Map<String, Integer> counterAlgo = new HashMap<>();
		for (String algo: Global.getAlgoIndexToName()) {
			counterAlgo.put(algo, new Integer(0));
		}
		for (User n: neighbours) {
			String neighbourAlgo = n.getCurrAlgo().getAlgoString();
			counterAlgo.put(neighbourAlgo, counterAlgo.get(neighbourAlgo) + 1);
		}

		// Calculate the effect of the modifier and change the score
		// score = avgscore * P(same score in neighbours) * weighting
		for (Algorithm algo: usersAlgos) {
			double scoreModifier = avgScore;
			scoreModifier *= (double) counterAlgo.get(algo.getAlgoString())
					/ neighbours.size();
			inputScores.put(algo, inputScores.get(algo) +
					weighting * scoreModifier);
		}

		return inputScores;
	}

	// Returns the base score (precision = |intersect(rec, hidden)| / |rec|)
	// A number of matches, divided by the number of recommendations.
	private Map<Algorithm, Double> calcAlgosWeighting(
			Map<Algorithm, List<Long>> algoToRecs,
			Map<Algorithm, Double> algoScores) {

		// Count number of matches under each algo - based on number of
		// suggestions from entire neighbourhood, so shouldn't disadvantage
		// many nodes.
		Set<Algorithm> algoSet = algoToRecs.keySet();
		for (Algorithm algo: algoSet) {
			List<Long> algosRecs = algoToRecs.get(algo);

			// Count number of rec to false hidden matches for each algo
			Double numMatchesForAlgo = 0.0;
			for (Long rec: algosRecs) {
				if (falseHidItems.contains(rec)) numMatchesForAlgo++;
			}

			// Calculate the precision
			Double score = numMatchesForAlgo / algosRecs.size();

			// Add the score on culmatively.
			if (algoScores.containsKey(algo)) {
				score += algoScores.get(algo);
			}

			algoScores.put(algo, score);
		}

		return algoScores;
	}

	private Map<Algorithm, List<Long>> getRecs(Map<Algorithm, Map<User, Double>> algoNeighbourhoods) {
		Set<Entry<Algorithm, Map<User, Double>>> entrySet = algoNeighbourhoods.entrySet();
		Map<Algorithm, List<Long>> algoToRecs = new HashMap<>(entrySet.size());

		for (Entry<Algorithm, Map<User, Double>> entry: entrySet) {
			algoToRecs.put(entry.getKey(),
					evaluater.createRecs(user, falseVisItems, entry.getValue()));
		}

		return algoToRecs;
	}

	private void generateFalseHiddenItems() {
		List<Collection<Long>> itemsArrays = user.hideSomeItems(userVisItems);
		falseVisItems = itemsArrays.get(0);
		falseHidItems = itemsArrays.get(1);
	}
}
