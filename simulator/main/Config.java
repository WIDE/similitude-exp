package main;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/*
 * To add parameters to this class you have to:
 * - add a private static variable
 * - assign the variable in init()
 * - add a getter with checkInitialised() as the first line of the method
 * - add value tests for safety
 * - update DatabaseHandler.java, specifically createTables() and insertConfig()
 * - add a line to the config file / update the config file generating script
 */
public class Config {
	// Internal tools
	private static Properties props;      // java.util.Properties, allows easy config file parsing
	private static boolean isInitialised = false; // Ensure init() is called only once

	// Unique simulation identifier
	private static String simulationId;   // A unique simulation id to differentiate simulations when merging output databases

	// Parameters
	private static int numRuns;                   // Number of times the simulator should run the entire gossip clustering
	private static int numRounds;                 // Number of rounds of each run
	private static int neighbourhoodSize;         // Number of neighboors of each user
	private static double fractionHiddenItems;    // Fraction of items to hide

	// Dataset reader
	private static String datasetType;    // Type of dataset. Possible values: refined, twitter, movielens, checkoptimal.
	private static String inputFilename;  // Dataset to read from

	// Optimal metrics checker
	private static String secondInputFilename;    // Second dataset to read from when checking optimal metrics

	// Dataset refiner
	private static int minSubscribers;            // Minimum number of users subscribing to an item for it to not be removed
	private static double ratingThreshold;        // Threshold to separate good and bad rating (i.e., threshold of 3 will consider 3.0 and 4.0 as good ratings)

	// File writer for calculations
	private static String outputFilename;         // File to write to the refined dataset

	// Database writer for simulations
	private static int dbBatchSize;               // The size of the write batches in the database
	private static String dbConnector;            // The type of the accessed database (sqlite, mysql, ...)
	private static String dbUrl;                  // The JDBC url (g.g., "jdbc:sqlite:path.db")

	// Free choice
	private static boolean staticAlgoForced;      // Prevent the adaptation, the first algorithm used by each user will remain his used algorithm for the duration of each entire run Used in CalcMain and in Main for Heterog Random and Heterog Optimal.
	private static String algoToStartWith;        // Start every user with this algorithm. Possible values: random (default), bestalgo, big, jaccard, overbig and overlap
	private static int numTimesToSwitchFalseItems;// Number of different sets of false hidden items to try, to ensure equality. Recommended higher, just reduced for test runs

	// Algorithms modifiers
	private static boolean withDetCurrAlgo;       // Activate the "detriment current algorithm" modifier
	private static boolean withIncPrevRounds;     // Activate the "increase previous rounds" modifier
	private static boolean withIncSimNodes;       // Activate the "increase similar nodes" modifier
	private static double detCurrAlgoWeighting;   // Weighting associated with detCurrAlgo
	private static double incPrevRoundsWeighting; // Weighting associated with incPrevRounds
	private static double incSimNodesWeighting;   // Weighting associated with incSimNodes
	private static double detrimentValue;         // Value to detriment when detCurrAlgo is activated (0 <= value <= 1)
	private static double incPrevRoundsMaxWeight; // Maximum weight used in incPrevRounds, weight for the most recent round
	private static int numPrevRoundsToInc;        // Number of rounds to consider in incPrevRounds
	private static int roundsOfCoolOff;           // Number of rounds of cool off between each algorithm decision
	private static double metricMargin;           // Precision margin of the piloting metric (0 <= value <= 1)

	// Multithreading
	private static int threadpoolSize;		      // Maximum number of concurrent threads (note: the higher the number of concurrent threads is, the higher the memory usage is too)
	private static int threadsTimeout;            // Time to wait for the thread completion in seconds. Can be followed by a suffix: m|M for minutes, h|H for hours, d|D for days

	// Debug
	private static boolean withDebugPrinting;     // Print extra information, it's better to use it with only one thread
	private static boolean withStaticSeed;        // Use a static seed to disable randomness
	private static boolean withMaxNodes;          // Enable the maxNodes feature
	private static int maxNodes;                  // Maximum number of nodes to use from the dataset

	/*
	 * Initialisation phase
	 */

	// Initialise the variables by reading the given file
	public static void init(String configFilename) {
		// Cannot call this function more than once
		if (isInitialised) {
			return;
		}
		isInitialised = true;

		if (configFilename == "") {
			throw new RuntimeException("Need a config file to read from.");
		}

		props = new Properties();

		try {
			configFilename = Paths.get(configFilename).toAbsolutePath().toString();
			props.load(new FileInputStream(configFilename));
		} catch (Exception e) {
			throw new RuntimeException("Config file loading error.", e);
		}

		readFile();
		checkValues();
	}

	// Assign the values from the config file to the class variables
	private static void readFile() {
		simulationId              = props.getProperty("simulationId");

		numRuns                   = readInt("numRuns");
		numRounds                 = readInt("numRounds");
		neighbourhoodSize         = readInt("neighbourhoodSize");
		fractionHiddenItems       = readDouble("fractionHiddenItems");

		datasetType               = readDatasetType("datasetType");
		inputFilename             = props.getProperty("inputFilename");

		secondInputFilename       = props.getProperty("secondInputFilename");

		minSubscribers            = readInt("minSubscribers");
		ratingThreshold           = readDouble("ratingThreshold");

		outputFilename            = props.getProperty("outputFilename");

		dbBatchSize               = readInt("dbBatchSize");
		dbConnector               = props.getProperty("dbConnector");
		dbUrl                     = props.getProperty("dbUrl");

		staticAlgoForced          = readBoolean("staticAlgoForced");
		algoToStartWith           = readStartAlgo("algoToStartWith");
		numTimesToSwitchFalseItems = readInt("numTimesToSwitchFalseItems");

		withDetCurrAlgo           = readBoolean("withDetCurrAlgo");
		withIncPrevRounds         = readBoolean("withIncPrevRounds");
		withIncSimNodes           = readBoolean("withIncSimNodes");
		detCurrAlgoWeighting      = readDouble("detCurrAlgoWeighting");
		incPrevRoundsWeighting    = readDouble("prevRoundsWeighting");
		incSimNodesWeighting      = readDouble("simNodesWeighting");
		detrimentValue            = readDouble("detrimentValue");
		incPrevRoundsMaxWeight    = readDouble("incPrevRoundsMaxWeight");
		numPrevRoundsToInc        = readInt("numPrevRoundsToInc");
		roundsOfCoolOff           = readInt("roundsOfCoolOff");
		metricMargin              = readDouble("metricMargin");

		threadpoolSize            = readInt("threadpoolSize");
		threadsTimeout            = readTime("threadsTimeout");

		withDebugPrinting         = readBoolean("withDebugPrinting");
		withStaticSeed            = readBoolean("withStaticSeed");
		withMaxNodes              = readBoolean("withMaxNodes");
		maxNodes                  = readInt("maxNodes");
	}

	// Verify some of the values given in the config file
	private static void checkValues() {
		// Force usage of static metrics for the refinement
		if (!datasetType.equals("refined")) {
			staticAlgoForced = true;
		}

		// If the dataset doesn't exist
		if (!new File(Paths.get(inputFilename).toAbsolutePath().toString()).exists()) {
			throw new RuntimeException("File " + inputFilename + " doesn't exist.");
		}

		// If the second dataset doesn't exist
		if (datasetType.equals("checkoptimal")) {
			// If the dataset doesn't exist
			if (! new File(Paths.get(secondInputFilename).toAbsolutePath().toString()).exists()) {
				throw new RuntimeException("File " + secondInputFilename + " doesn't exist.");
			}
		}

		// If the database already exists
		if (datasetType.equals("refined")) {
			String dbFilename = Config.getDbUrl().split(":")[2];
			if (new File(Paths.get(dbFilename).toAbsolutePath().toString()).exists()) {
				throw new RuntimeException("File " + dbFilename + " already exists.");
			}
		}
	}

	// Read and convert a String into a boolean
	private static boolean readBoolean(String name) {
		String stringValue = props.getProperty(name);

		if (stringValue.equalsIgnoreCase("true") || stringValue.equals("1")) {
			return true;
		} else if (stringValue.equalsIgnoreCase("false") || stringValue.equals("0")) {
			return false;
		} else {
			throw new RuntimeException("In the config file: \"" + name + "\" is not a boolean.");
		}
	}

	// Read and convert a String into an integer
	private static int readInt(String name) {
		String stringValue = props.getProperty(name);

		try {
			return Integer.parseInt(stringValue);
		} catch (NumberFormatException e) {
			throw new RuntimeException("In the config file: \"" + name + "\" is not an integer.");
		}
	}

	// Read and convert a String into a double
	private static double readDouble(String name) {
		String stringValue = props.getProperty(name);

		try {
			return Double.parseDouble(stringValue);
		} catch (NumberFormatException e) {
			throw new RuntimeException("In the config file: \"" + name + "\" is not a double.");
		}
	}

	// Read and check the value of the dataset type
	private static String readDatasetType(String name) {
		String stringValue = props.getProperty(name).toLowerCase();

		List<String> possibleValues = new ArrayList<>();
		possibleValues.add("refined");
		possibleValues.add("twitter");
		possibleValues.add("movielens");
		possibleValues.add("checkoptimal");

		// Return when one value is found
		for (String algo : possibleValues) {
			if (stringValue.equals(algo)) {
				return stringValue;
			}
		}

		// Error message otherwise
		String msg = "In the config file: " + name + " should have one of the "
				+ "following values: "+ possibleValues;
		throw new RuntimeException(msg);
	}

	// Read and check the value of the start algo
	private static String readStartAlgo(String name) {
		String stringValue = props.getProperty(name).toLowerCase();

		List<String> possibleValues = new ArrayList<>();
		possibleValues.add("");
		possibleValues.add("random");
		possibleValues.add("bestalgo");
		possibleValues.addAll(Arrays.asList(Global.getAlgoIndexToName()));

		// Return when one value is found
		for (String algo : possibleValues) {
			if (stringValue.equals(algo)) {
				return stringValue;
			}
		}

		// Error message otherwise
		String msg = "In the config file: " + name + " should have one of the "
				+ "following values: "+ possibleValues;
		throw new RuntimeException(msg);
	}

	// Called when initializing the variables, convert a String containing an
	// integer with a suffix: m for minutes, h for hours, d for days.
	private static int readTime(String name) {
		String stringValue = props.getProperty(name).toLowerCase();
		int multiplier = 1;

		if (stringValue.endsWith("m")) {
			multiplier = 60;
		} else if (stringValue.endsWith("h")) {
			multiplier = 60*60;
		} else if (stringValue.endsWith("d")) {
			multiplier = 60*60*24;
		}

		try {
			if (multiplier == 1) {
				return Integer.parseInt(stringValue);
			} else {
				return Integer.parseInt(stringValue.substring(0, stringValue.length()-1)) * multiplier;
			}
		} catch (NumberFormatException e) {
			throw new RuntimeException("In the config file: \"" + name + "\" is not in a correct format. " +
					"Expected format: <integer>[mMhHdD] (minutes, hours, days).");
		}
	}

	// checkInitialised throws an Exception if the Config class has not been
	// initialised before. Called by the getters.
	private static void checkInitialised() {
		if (!isInitialised) {
			throw new RuntimeException("Call Config.init() before using the getters");
		}
	}

	/*
	 * Getters
	 */

	public static String getSimulationId() {
		checkInitialised();
		return simulationId;
	}

	public static int getNumRuns() {
		checkInitialised();
		return numRuns;
	}

	public static int getNumRounds() {
		checkInitialised();
		return numRounds;
	}

	public static int getNeighbourhoodSize() {
		checkInitialised();
		return neighbourhoodSize;
	}

	public static double getFractionHiddenItems() {
		checkInitialised();
		return fractionHiddenItems;
	}

	public static String getDatasetType() {
		checkInitialised();
		return datasetType;
	}

	public static String getInputFilename() {
		checkInitialised();
		return inputFilename;
	}

	public static String getSecondInputFilename() {
		checkInitialised();
		return secondInputFilename;
	}

	public static int getMinSubscribers() {
		checkInitialised();
		return minSubscribers;
	}

	public static double getRatingThreshold() {
		checkInitialised();
		return ratingThreshold;
	}

	public static String getOutputFilename() {
		checkInitialised();
		return outputFilename;
	}

	public static int getDbBatchSize() {
		checkInitialised();
		return dbBatchSize;
	}

	public static String getDbConnector() {
		checkInitialised();
		return dbConnector;
	}

	public static String getDbUrl() {
		checkInitialised();
		return dbUrl;
	}

	public static boolean isStaticAlgoForced() {
		checkInitialised();
		return staticAlgoForced;
	}

	public static String getAlgoToStartWith() {
		checkInitialised();
		return algoToStartWith;
	}

	public static int getNumTimesToSwitchFalseItems() {
		checkInitialised();
		return numTimesToSwitchFalseItems;
	}

	public static boolean withDetCurrAlgo() {
		checkInitialised();
		return withDetCurrAlgo;
	}

	public static boolean withIncPrevRounds() {
		checkInitialised();
		return withIncPrevRounds;
	}

	public static boolean withIncSimNodes() {
		checkInitialised();
		return withIncSimNodes;
	}

	public static double getDetCurrAlgoWeighting() {
		checkInitialised();
		return detCurrAlgoWeighting;
	}

	public static double getIncPrevRoundsWeighting() {
		checkInitialised();
		return incPrevRoundsWeighting;
	}

	public static double getIncSimNodesWeighting() {
		checkInitialised();
		return incSimNodesWeighting;
	}

	public static double getDetrimentValue() {
		checkInitialised();
		return detrimentValue;
	}

	public static double getIncPrevRoundsMaxWeight() {
		checkInitialised();
		return incPrevRoundsMaxWeight;
	}

	public static int getNumPrevRoundsToInc() {
		checkInitialised();
		return numPrevRoundsToInc;
	}

	public static int getRoundsOfCoolOff() {
		checkInitialised();
		return roundsOfCoolOff;
	}

	public static double getMetricMargin() {
		checkInitialised();
		return metricMargin;
	}

	public static int getThreadpoolSize() {
		checkInitialised();
		return threadpoolSize;
	}

	public static int getThreadsTimeout() {
		checkInitialised();
		return threadsTimeout;
	}

	public static boolean withDebugPrinting() {
		checkInitialised();
		return withDebugPrinting;
	}

	public static boolean withStaticSeed() {
		checkInitialised();
		return withStaticSeed;
	}

	public static boolean withMaxNodes() {
		checkInitialised();
		return withMaxNodes;
	}

	public static int getMaxNodes() {
		checkInitialised();
		return maxNodes;
	}
}
