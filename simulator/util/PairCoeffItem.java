package util;


// Tuple of size 2 to use with a PriorityQueue
public class PairCoeffItem implements Comparable<PairCoeffItem> {
	private Double coeff;
	private Long item;
	private int hash;

	public PairCoeffItem(Double coeff, Long item) {
		this.coeff = coeff;
		this.item = item;
		computeHash();
	}

	// Cache the hash to avoid computing it in each comparison
	// We just want its value to not prioritise one item over another
	private void computeHash() {
		// Much faster than this.hashCode(), but is not a perfect hash at all
		hash = item.hashCode();
	}

	public Double getCoeff() {
		return coeff;
	}

	public Long getItem() {
		return item;
	}

	// This function does not follow the specification
	// Two pairs with the same (coeff, item) should never be compared, we just
	// want different items with the same coeff to have a random order
	public int compareTo(PairCoeffItem o) {
		// Order with the coeff
		if (this.coeff > o.coeff) return 1;
		if (this.coeff < o.coeff) return -1;

		// From this point on, coeffs are equals
		// The coeff is not enough to have a strict order, we then use a hash to
		// randomise the items who have the same coeffs
		if (this.hash > o.hash) return 1;
		if (this.hash < o.hash) return -1;

		// In case the hash is the same, arbitrarily return 1
		System.out.println("Warning: PairCoeffItem: compareTo(): shouldn't happen!");
		return 1;
	}
}
