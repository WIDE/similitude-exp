package util;

import java.util.Comparator;
import java.util.Map;

import main.User;

// Enables a TreeMap to have the biggest coeff at its head
public class ComparatorCoeffUser implements Comparator<User> {
	private Map<User, Double> base;

	public ComparatorCoeffUser(Map<User, Double> base) {
		this.base = base;
	}

	public int compare(User o1, User o2) {
		Double coeff1 = base.get(o1);
		Double coeff2 = base.get(o2);

		// Order with the user's coeff
		if (coeff1 < coeff2) return 1;
		if (coeff1 > coeff2) return -1;

		// From this point on: base.get(a) == base.get(b)
		// Coeff is not enough to have a strict order to match
		// Comparator.compare() specification, we then use the hash of the
		// user to randomise user who have the same coeff
		if (o1.hashCode() < o2.hashCode()) return 1;
		if (o1.hashCode() > o2.hashCode()) return -1;

		// From this point on: a.hashCode() == b.hashCode()
		// The two tuples are the same object
		return 0;
	}
}
