package calculations;

import io.RefinedReader;

import java.util.List;

import main.Config;
import main.Global;
import main.User;

// Main class used for detecting differences in two refined datasets
public class CheckOptimalMain {
	// Lists of users
	private static List<User> users1;
	private static List<User> users2;

	public static void checkOptimalMain() {
		readUsers();
		checkOptimal();
	}

	// Read users in from the json files, fill the users variables
	private static void readUsers() {
		System.out.println("---- Begin input file loading");

		// Refined dataset reader
		users1 = new RefinedReader().read(Config.getInputFilename());
		users2 = new RefinedReader().read(Config.getSecondInputFilename());

		// Check size difference
		if (users1.size() != users2.size()) {
			System.out.println("Datasets don't have the same size: " +
					users1.size() + ", " + users2.size());
			System.out.println("Aborting program.");
		}

		System.out.println("Num users read:\t" + users1.size());
		System.out.println("---- End of input file loading");
		System.out.println();
	}


	// Check if the best algos of each user match across datasets
	private static void checkOptimal() {
		// Stores the number of times the list of bestAlgos match
		int numPerfectMatches = 0;
		int numPartialMatches = 0;
		int numItemsNotMatching = 0;

		for (int i = 0; i < users1.size(); i++) {
			User u1 = users1.get(i);
			User u2 = users2.get(i);
			List<String> ba1 = u1.getBestAlgos();
			List<String> ba2 = u2.getBestAlgos();

			boolean perfectMatch = false;
			boolean partialMatch = false;

			if (ba1.containsAll(ba2) ||	ba2.containsAll(ba1)) {
				if (ba1.size() == ba2.size()) {
					// Perfect match if both BA set are equals
					perfectMatch = true;
				} else {
					// Partial match if one BA set contains the other BA set
					partialMatch = true;
				}
			}

			if (perfectMatch) {
				numPerfectMatches++;
			}
			if (partialMatch) {
				numPartialMatches++;
			}

			// Compute the average number of items per user if there is no
			// match found in the best algos
			// (suspecting very low amount of items)
			if (!perfectMatch && !partialMatch) {
				numItemsNotMatching += u1.getHiddenItems().size() +
						u1.getVisItems().size();
			}
		}

		double percentagePerfectMatches = (double) numPerfectMatches
				/ Global.getNumNodes() * 100;
		double percentagePartialMatches = (double) numPartialMatches
				/ Global.getNumNodes() * 100;
		int avgItemsNotMatching = (numItemsNotMatching
				/ (Global.getNumNodes() - numPerfectMatches - numPartialMatches));

		System.out.println("Number of nodes where BestAlgos matched "
				+ "perfectly across both runs:\t" + numPerfectMatches + "\t"
				+ String.format(Global.getLocale(),	"%.3f",
						percentagePerfectMatches) + "%");

		System.out.println("Number of nodes where BestAlgos matched "
				+ "partially across both runs:\t" + numPartialMatches + "\t"
				+ String.format(Global.getLocale(), "%.3f",
						percentagePartialMatches) + "%");

		if (numItemsNotMatching != 0) {
			System.out.println("Average number of items per users for the "
					+ "ones not matching:\t" + avgItemsNotMatching);
		}

		System.out.println();
	}

}
