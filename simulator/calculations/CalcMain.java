package calculations;

import io.MovieLensReader;
import io.RefinedWriter;
import io.TwitterFoursquareReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import main.Clusterer;
import main.Config;
import main.Global;
import main.User;

// Main class used for refining a dataset
public class CalcMain {
	// List of users, it needs to be a list to be able to pick random users from it
	private static List<User> users;

	// avgPerAlgo contains the precision average of the several runs for
	// each algo and for each user
	// Map<String> = map of username,
	// Map<String, Double> = map of precision average of the several runs
	// for each algo (String = algoname)
	private static Map<String, Map<String, Double>> avgPerAlgo;


	// Main for the dataset refinement
	public static void calcMain() {
		// Read users in
		readUsers();

		// Refine the dataSet if not already refined
		refineDataset();

		// Main loop, run the clusters and the evaluation
		runClustersAndEval();

		// Compute the best algorithms for each user and store it
		computeBestAlgos();

		// Print some end of simulation stats
		printEndStats();

		// Write all users out into new JSON file
		RefinedWriter writer = new RefinedWriter();
		writer.writeOut(users, Config.getOutputFilename());
		System.out.println("Results in file(s): " + Config.getOutputFilename());
	}

	// Read users in from the json file, fill the users variable
	private static void readUsers() {
		System.out.println("---- Begin input file loading");

		// For Twitter-Foursquare dataset
		if (Config.getDatasetType().equals("twitter")) {
			users = new TwitterFoursquareReader().read(Config.getInputFilename());
		}

		// For MovieLens dataset
		if (Config.getDatasetType().equals("movielens")) {
			users = new MovieLensReader().read(Config.getInputFilename());
		}

		// For Yell.com dataset
		// users = new YellJSONReader().read(Config.getInputFilename());

		System.out.println("Num users read:\t" + users.size());
		System.out.println("---- End of input file loading");
		System.out.println();
	}

	// Refine the dataset if not already refined
	private static void refineDataset() {
		System.out.println("---- Begin dataset refining");
		DataSetRefiner dsr = new DataSetRefiner(users);
		dsr.removeUnpopularItems();
		dsr.removeUsersNoHiddenItems();

		// Print basic informations about the dataset: number of users and
		// average number of items per user
		int counter = 0;
		for (User u: users) {
			counter += u.getVisItems().size() + u.getHiddenItems().size();
		}

		// Compute the average number of items
		int avg = counter/users.size();
		Global.setAverageNumItems(avg);
		System.out.println("Average number of items:\t\t" + avg);
		System.out.println("---- End of dataset refining");
		System.out.println();
	}

	// Run clusters and the evaluation for each algo and each run in several
	// threads, and averages the results
	private static void runClustersAndEval() {
		// Initialise the results store
		avgPerAlgo = new HashMap<>();
		for (User u: users) {
			avgPerAlgo.put(u.getName(), new HashMap<String, Double>());
		}

		// Create eval object to average the results
		CalcEval calcEval = new CalcEval(users);

		// resToAvg contains the results of a single run before being averaged
		// List = list of algos, String = username, List<Double> = list of precisions, 1 per run
		List<Map<String, List<Double>>> resToAvg = new ArrayList<>();
		for (int algoIndex = 0; algoIndex < Global.getNumAlgosAvailable(); algoIndex++) {
			resToAvg.add(new HashMap<String, List<Double>>());
			Map<String, List<Double>> resToAvgAlgo = resToAvg.get(algoIndex);
			for (User u: users) {
				resToAvgAlgo.put(u.getName(), new ArrayList<Double>());
			}
		}

		// Create the thread pool and set the max threads supported
		ExecutorService pool = Executors.newFixedThreadPool(Config.getThreadpoolSize());

		System.out.println("Num algos:\t"   + Global.getNumAlgosAvailable());
		System.out.println("Num runs:\t"    + Config.getNumRuns());
		System.out.println("Num rounds:\t"  + Config.getNumRounds());
		System.out.println("Num threads:\t" + Config.getThreadpoolSize());
		System.out.println("=> Max useful threads:\t" +
				(Config.getNumRuns() * Global.getNumAlgosAvailable()));
		System.out.println("=> Total rounds:\t" +
				(Config.getNumRuns() * Config.getNumRounds() * Global.getNumAlgosAvailable()));
		System.out.println();
		System.out.println("---- Begin simulation");

		// Add all the runs at once in the executor, they will be executed when
		// a thread is available
		// Each algorithm is ran several times
		for (int algoIndex = 0; algoIndex < Global.getNumAlgosAvailable(); algoIndex++) {
			for (int numRun = 0; numRun < Config.getNumRuns(); numRun++) {
				// Inner class just to be able to launch several methods in the
				// same thread, the initialization is a bit of a hack...
				Thread t = new Thread() {
					private Map<String, List<Double>> globalResToAvg;
					private List<User> localUsers, globalUsers;
					private CalcEval localCalcEval;
					private int algoIndex;
					private int numRun;

					Thread initialize(List<User> globalUsers,
							Map<String, List<Double>> globalResToAvg,
							int algoIndex, int runNum) {
						this.globalUsers = globalUsers;
						this.globalResToAvg = globalResToAvg;
						this.algoIndex = algoIndex;
						this.numRun = numRun;
						return this;
					}

					@Override
					public void run() {
						// Duplicate the users and calcEval to have a thread-safe environment
						localUsers = new ArrayList<>();
						for (User u: globalUsers) {
							User clonedUser = new User(u);
							// Make the user start with that algorithm
							clonedUser.algoChange(algoIndex);
							localUsers.add(clonedUser);
						}
						localCalcEval = new CalcEval(localUsers);

						// Run gossiping to cluster users
						new Clusterer(localUsers, localCalcEval, numRun).cluster();

						// Gather the evaluations in resToAvg
						localCalcEval.runEval(globalResToAvg);
					}

				}.initialize(users, resToAvg.get(algoIndex), algoIndex, numRun);

				pool.execute(t);
			}
		}

		// Let the tasks finish and deny future entrance
		pool.shutdown();

		// Wait for all the runs to terminate
		try {
			boolean tasksFinished = pool.awaitTermination(Config.getThreadsTimeout(), TimeUnit.SECONDS);
			if (!tasksFinished) {
				System.out.println("Simulation aborted by timeout.");
				System.out.println("Exiting program.");
				System.exit(1);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("---- End of simulation");
		System.out.println();

		// Average the results from the different runs
		for (int algoIndex = 0; algoIndex < Global.getNumAlgosAvailable(); algoIndex++) {
			calcEval.avgOutResults(resToAvg.get(algoIndex), avgPerAlgo, Global.getAlgoIndexToName()[algoIndex]);
		}
	}

	// Compute the best algorithms for each user and store it
	private static void computeBestAlgos() {
		// Output best Algo and fixed set of hidden items to the json file
		for (User u: users) {
			Map<String, Double> scores = avgPerAlgo.get(u.getName());
			List<String> bestAlgos = new ArrayList<>();
			double bestAlgoScore = 0.0;

			// Find the best score
			for (Double score: scores.values()) {
				if (score > bestAlgoScore) {
					bestAlgoScore = score;
				}
			}

			// Add the Algorithm that are within the best algorithm score's margin
			for (String algoName: scores.keySet()) {
				Double score = scores.get(algoName);

				boolean isInMargin = (bestAlgoScore - score) <= (bestAlgoScore * Config.getMetricMargin());
				if ((score <= bestAlgoScore) && isInMargin ) {
					bestAlgos.add(algoName);
				}
			}

			// Check consistency: an algorithm shouldn't be present twice in bestAlgos
			Set<String> duplicateCheck = new HashSet<>();
			for (String algoName: bestAlgos) {
				if (duplicateCheck.contains(algoName)) {
					System.out.println("WARNING: node " + u.getName() + " has" +
							" multiple instances of the algorithm: " + algoName);
				} else {
					duplicateCheck.add(algoName);
				}
			}

			// Set the best algorithms on the user objects
			u.setBestAlgo(bestAlgos);
		}
	}

	// Print the number of user with X best algos and the number of hits of each
	// algorithm as best algos
	private static void printEndStats() {
		// Init variables
		double toPercentage = (double) 100 / Global.getNumNodes();
		int[] counterBA = new int[Global.getNumAlgosAvailable()];
		Map<String, Integer> counterAlgo = new TreeMap<>();
		for (String algo: Global.getAlgoIndexToName()) {
			counterAlgo.put(algo, new Integer(0));
		}

		// Fill the variables
		for (User u: users) {
			List<String> ba = u.getBestAlgos();
			counterBA[ba.size()-1]++;
			for (String algo: ba) {
				counterAlgo.put(algo, counterAlgo.get(algo) + 1);
			}
		}

		// Print the number of user with X best algos
		for (int i = 0; i < counterBA.length; i++) {
			System.out.println("Num user with " + (i+1) + " best algos:\t" +
					String.format(Global.getLocale(), "%.3f", counterBA[i] * toPercentage));
		}
		System.out.println();

		// Print the number of hits in best algos of each algo for each user
		for (String algo: counterAlgo.keySet()) {
			int count = counterAlgo.get(algo);
			System.out.println("Num times " + algo + " is in best algos:\t" +
					String.format(Global.getLocale(), "%.3f", count * toPercentage));
		}
		System.out.println();
	}
}
