package calculations;

import java.util.List;
import java.util.Map;

import main.Evaluater;
import main.User;

public class CalcEval extends Evaluater {

	// To protect resToAvg from concurrent access in runEval()
	private static Object lock1 = new Object();

	public CalcEval(List<User> users) {
		super(users);
	}

	// Method stores results of a single full run under an algorithm
	public void runEval(Map<String, List<Double>> resToAvg) {
		for (User u: super.getUsers()) {
			List<Long> recs = createRecs(u, u.getVisItems(), getCoeffsPerNeighFromUserStore(u, u.getNeighbours(), u.getCurrAlgo()));
			synchronized (lock1) {
				resToAvg.get(u.getName()).add(calcPrecision(u, recs));
			}
		}
	}

	// Method averages out X runs of algorithm and stores it in the avgPerAlgo map
	public void avgOutResults(Map<String, List<Double>> resToAvg, Map<String, Map<String, Double>> avgPerAlgo, String algoName) {
		for (User u: super.getUsers()) {
			List<Double> results = resToAvg.get(u.getName());
			Double average = 0.0;
			for (Double result: results) {
				average += result;
			}
			average = average / results.size();
			avgPerAlgo.get(u.getName()).put(algoName, average);
		}
	}
}
