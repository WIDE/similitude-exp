package calculations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import main.Config;
import main.Global;
import main.User;

// Class to refine the dataSet, and reduce it to only well known items,
// to ease processing, and allow better recs?
// Consider how this applies to RL situation...
public class DataSetRefiner {
	private List<User> users;

	public DataSetRefiner(List<User> users) {
		this.users = users;
	}

	// Remove all users left with no hidden items
	public void removeUsersNoHiddenItems() {
		System.out.println("Num users:\t" + users.size());
		System.out.println("Removing all users with no items...");

		for (int i = 0; i < users.size(); i++) {
			User u = users.get(i);
			if (u.getHiddenItems().size() < 2 && u.getVisItems().size() < 5) {
				users.remove(i);
				// Just removed index i, need to recompute this index
				i--;
			}
		}
		System.out.println("Number of users remaining:\t" + users.size());
		Global.setNumNodes(users.size());
	}

	// Remove all items known about by less than X users (X set in Config)
	// Don't use this method with fixed hidden items, intrinsically shuffles them up
	public void removeUnpopularItems() {
		Map<Long, List<User>> mapItems = new HashMap<>();
		Collection<Long> userItems;
		int numItems = 0;
		int numUniqueItems = 0;

		System.out.println("Removing unpopular items...");

		// Get all items into a Hashmap with num items
		for (User user: users) {
			// Get all items in one collection
			userItems = new HashSet<>(user.getVisItems());
			userItems.addAll(user.getHiddenItems());

			for (Long item: userItems) {
				numItems++;

				if (mapItems.containsKey(item)) {
					// Update hashmap if present
					List<User> itemsUsers = mapItems.get(item);
					itemsUsers.add(user);
					mapItems.put(item, itemsUsers);
				} else {
					// Put in hashmap if not present
					List<User> newList = new ArrayList<>();
					newList.add(user);
					mapItems.put(item, newList);
					numUniqueItems++;
				}
			}
		}
		System.out.println("Total num items:\t" + numItems);
		System.out.println("Total num unique items:\t\t" + numUniqueItems);

		// Remove items that are known by less than X users (set in Config)
		int threshold = Config.getMinSubscribers();
		int numItemsRemoved = 0;

		for (Long item: mapItems.keySet()) {
			List<User> usersForCurrItem = mapItems.get(item);

			if (usersForCurrItem.size() < threshold) {
				numItemsRemoved++;

				// Remove item from all users that contain it
				for (User user: usersForCurrItem) {
					user.getVisItems().remove(item);
					user.getHiddenItems().remove(item);
				}
			}
		}
		System.out.println("Number of items removed:\t" + numItemsRemoved);

		// Remove users with 0 or 1 items
		for (int i = 0; i < users.size(); i++) {
			User u = users.get(i);
			int allItemsSize = u.getVisItems().size() + u.getHiddenItems().size();

			if (allItemsSize < 2) {
				users.remove(i);
				// Just removed index i, need to recompute this index
				i--;
			} else {
				userItems = new HashSet<>(u.getVisItems());
				userItems.addAll(u.getHiddenItems());

				// Update all hidden/visible items to clear any inconsistencies
				List<Collection<Long>> items = u.hideSomeItems(userItems);
				u.setVisibleItems(items.get(0));
				u.setHiddenItems(items.get(1));
			}
		}
	}
}
