$ grep -FRn 'TODO' .
$ grep -FRn 'NOTE' .

Simulator
----------
- Put all the method headers between /**/ instead of // and remove their name
  from the comment
- Better exceptions, don't use RuntimeException everywhere
- Implement a dataset reader for the anonymized version of the Twitter/Foursquare dataset
