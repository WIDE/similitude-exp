#! /bin/bash
# Copy the .csv from a result directory to a fixed temporary directory so that
# the graph creating script can work

# Global constants and functions
cd $(dirname "$0")
. global.sh
cd "${OLDPWD}"


##### Constants
# Filename translation map
declare -A names
names["similitude_allmodifiers"]="1"
names["similitude_basic"]="2"
names["static_bestalgo"]="3"
names["similitude_detcurralgo"]="5"
names["similitude_incprevrounds_weight0.5"]="7"
names["similitude_incsimnodes_weight0.5"]="8"
names["static_random"]="9"
names["static_big"]="10"
names["static_jaccard"]="11"
names["static_overbig"]="12"
names["static_overlap"]="13"
names["similitude_allmodifiers_cooloff0"]="18"
names["similitude_allmodifiers_cooloff5"]="19"
#####

function usage() {
	message="\
Usage: $(basename $0) <csv dir> [options...]
Copy the .csv from a result directory to a fixed temporary directory so that
the graph creating script can work

Parameters:
    <csv dir>
        directory containing the csv to use for the plots

Script options:
    -h, --help
        display this message\
"

	echo "${message}"
	exit 0
}


# First check if only the help is asked
case "$1" in
    -h|--help|help|"") usage;;
esac

# Verify the number of arguments
if [[ $# -lt 1 ]]; then
    error "Missing the argument: csv directory"
fi

# Assign the firsts arguments
dir_csv="$1"

# Verify the simulation id value (non empty string and 20 characters max)
if [[ ! -d "${dir_csv}" ]]; then
    error "The given csv directory needs to be an existing directory: ${dir_csv}"
fi

# Create the temporary directory if needed
mkdir -p "${dir_tmp_csv}"

# Move the csv files according to the nams dictionary
for file in "${!names[@]}"; do
	# Copy the csv file to a name understood by the plot creating script
	cp "${dir_csv}/${file}.csv" "${dir_tmp_csv}/${names[${file}]}.csv" && \
		echo "Copied ${file}.csv;-> ${names[${file}]}.csv" 
done | column -t -s ';'

# Create a csv file containing the weightings results of a modifier
function create_csv_weight () {
    modifier="$1"
    new_file="$2"

    # Copy the basic simulation as the "weight 0.0"
    cp -f "${dir_csv}/similitude_basic.csv" "${dir_csv}/similitude_${modifier}_weight0.0.csv"

    # Create the csv header
    echo "modifier,weight,recall,precision,nodes_switching,nodes_on_best_algos" > "${dir_tmp_csv}/${new_file}"

    # Fill the new csv with the last round results of all the weights
    for file in "${dir_csv}/similitude_${modifier}_weight"*.csv; do
        results=$(tail -n1 ${file} | cut -d',' -f3-)
        weight=$(echo ${file} | sed -r "s/^.*similitude_${modifier}_weight(.*)\.csv$/\1/")

        echo "${modifier},${weight},${results}" >> "${dir_tmp_csv}/${new_file}"
    done

    echo "Created ${new_file} (weighting ${modifier})"
}

# Create the csv files for the weighting sensibiliy graph
create_csv_weight "incprevrounds" "20.csv"
create_csv_weight "incsimnodes" "21.csv"
