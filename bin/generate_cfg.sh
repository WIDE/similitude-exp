#! /bin/bash
# Generate a config file to perform one simulation under the given parameters

# Global constants and functions
cd $(dirname "$0")
. global.sh
cd "${OLDPWD}"


##### Constants
db_url_root="jdbc:sqlite"
#####

##### Config options
# Unique simulation identifier
simulation_id=will_be_filled

# Parameters
runs=10
rounds=100
neighboorhood_size=10
fraction_hidden_items=0.2

# Dataset reader
dataset_type=will_be_filled
path_dataset=will_be_filled

# Optimal metrics checker
path_second_dataset=will_be_filled

# Dataset refiner
min_subscribers=20
rating_threshold=3

# File writer for calculations
output_filename=will_be_filled

# Database writer for simulations
db_connector=org.sqlite.JDBC
db_url=will_be_filled
db_batch_size=10000

# Free choice
static=false
start_algo=random
times_switch_false_items=3

# Algorithms modifiers
detcurralgo=false
incprevrounds=false
incsimnodes=false
detcurralgo_weighting=1.0
incprevrounds_weighting=0.5
incsimnodes_weighting=0.5
detriment_value=0.1
incprevrounds_max_weight=0.5
prev_rounds=5
cool_off=2
metric_margin=0.00

# Multithreading
thread_pool_size=10
threads_timeout=2d

# Debug
debug_printing=false
static_seed=false
with_max_nodes=false
max_nodes=500
#####


function usage() {
    message="\
Usage: $(basename $0) <dataset type> <simulation id> <dataset path> [options...]
Generate a config file to perform one simulation under the given parameters

Parameters:
    <dataset type>
        possible values: refined, twitter, movielens, checkoptimal
    <simulation id>
        unique id to differentiate the simulations results (1 <= length <= 53)
    <dataset path>
        path of the dataset

Script options:
    -h, --help
        display this message

Simulation options:
    --runs INTEGER
        number of times the simulator should run the entire gossip clustering
    --rounds INTEGER
        number of rounds of each run
    --neighboorhoodSize INTEGER
        number of neighboors of each user
    --fractionHiddenItems FLOAT
        fraction of items to hide when splitting items in visible/hidden sets

    # Optimal metrics checker
    --secondDataset STRING
        path of the second dataset, to use with checkoptimal

    # Dataset refiner
    --minSubscribers INTEGER
        minimum number of users subscribing to an item for it to not be removed
    --ratingThreshold INTEGER
        threshold to separate good and bad rating

    # Database writer for simulations
    --dbBatchSize INTEGER
        the size of the write batches in the database
    --dbConnector STRING
        class of the accessed database

    # Free choice
    --static
        prevent the adapation, the first algorithm used by each user will
        remain his used algoithm for the duration of each entire run
        Used in SimuMain for Heterog Random and Heterog Optimal
    --startAlgo STRING
        start every user with this algorithm, possible values: random
        (by default), bestalgo, big, jaccard, overbig and overlap
    --timesSwitchFalseItems INTEGER
        number of different sets of false hidden items to try

    # Algorithms modifiers
    --allModifiers
        activate all the modifiers
    --detcurralgo
        activate the \"deter current algorithm\" modifier
    --incprevrounds
        activate the \"increase previous rounds\" modifier
    --incsimnodes
        activate the \"increase similar nodes\" modifier
    --detcurralgoWeighting FLOAT
        weighting associated with detCurrAlgo
    --incprevroundsWeighting FLOAT
        weighting associated with incPrevRounds
    --incsimnodesWeighting FLOAT
        weighting associated with incSimNodes
    --detrimentValue DOUBLE
        value to detriment when detCurrAlgo is activated (0 <= value <= 1)
    --incprevroundsMaxWeight FLOAT
        maximum weight used in incPrevRounds, weight for the most recent round
    --prevRounds INTEGER
        number of rounds to consider in incPrevRounds
    --coolOff INTEGER
        number of rounds of cool off between each algorithm decision
    --metricMargin FLOAT
        precision margin of the piloting metric (the higher, the more flexible)

    # Multithreading
    --threadPoolSize INTEGER
        maximum number of concurrent threads (note: the higher the
        number of concurrent threads is, the higher the memory usage is too)
    --threadsTimeout STRING
        time to wait for the thread completion, can be followed by
        a suffix: m|M for minutes, h|H for hours, d|D for days

    # Debug
    --debugPrinting
        print extra information, it's better to use with only one thread
    --staticSeed
        use a static seed to disable randomness
    --maxNodes INTEGER
        maximum number of nodes to use from the dataset\
"

    echo "${message}"
    exit 0
}


# First check if only the help is asked
case "$1" in
    -h|--help|help|"") usage;;
esac

# Verify the number of arguments
if [[ $# -lt 3 ]]; then
    error "Missing at least one arguments out of: dataset type, simulation id, dataset path"
fi

# Assign the firsts arguments
dataset_type="$1"
simulation_id="$2"
path_dataset="$3"

# Verify the dataset type value
case "${dataset_type}" in
    refined|twitter|movielens|checkoptimal) ;;
    *) error "The given dataset type has an unexpected value: ${dataset_type}";;
esac

# Verify the simulation id value (non empty string and 20 characters max)
simulation_id_max_length=$((64-1-${#date}))
if [[ -n "${simulation_id}" ]] && [[ "${#simulation_id}" -gt "${simulation_id_max_length}" ]]; then
    error "The given simulation id needs to be between 1 and ${simulation_id_max_length} characters long"
fi

# Parse the options
shift 3 # Remove the first arguments from the array
while [[ $# != 0 ]] ; do
    case "$1" in
        # Simulation options
        --runs)                   runs="$2";                    shift 1;;
        --rounds)                 rounds="$2";                  shift 1;;
        --neighboorhoodSize)      neighboorhood_size="$2";      shift 1;;
        --fractionHiddenItems)    fraction_hidden_items="$2";   shift 1;;
        --minSubscribers)         min_subscribers="$2";         shift 1;;
        --ratingThreshold)        rating_threshold="$2";        shift 1;;
        --static)                 static=true;                         ;;
        --startAlgo)              start_algo="$2";              shift 1;;
        --timesSwitchFalseItems)  times_switch_false_items="$2";shift 1;;
        --detcurralgo)            detcurralgo=true;                    ;;
        --incprevrounds)          incprevrounds=true;                  ;;
        --incsimnodes)            incsimnodes=true;                    ;;
        --detcurralgoWeighting)   detcurralgo_weighting="$2";   shift 1;;
        --incprevroundsWeighting) incprevrounds_weighting="$2"; shift 1;;
        --incsimnodesWeighting)   incsimnodes_weighting="$2";   shift 1;;
        --detrimentValue)         detriment_value="$2";         shift 1;;
        --incprevroundsMaxWeight) incprevrounds_max_weight="$2";shift 1;;
        --prevRounds)             prev_rounds="$2";             shift 1;;
        --coolOff)                cool_off="$2";                shift 1;;
        --metricMargin)           metric_margin="$2";           shift 1;;
        --threadPoolSize)         thread_pool_size="$2";        shift 1;;
        --threadsTimeout)         threads_timeout="$2";         shift 1;;
        --dbBatchSize)            db_batch_size="$2";           shift 1;;
        --dbConnector)            db_connector="$2";            shift 1;;
        --debugPrinting)          debug_printing=true;                 ;;
        --staticSeed)             static_seed=true;                    ;;
        --maxNodes)
            max_nodes="$2"
            shift 1
            with_max_nodes=true
            ;;
        --allModifiers)
            detcurralgo=true
            incprevrounds=true
            incsimnodes=true
            ;;
        --secondDataset)
            path_second_dataset="$2"
            shift 1
            ;;

        # Script options
        -h|--help)
            usage
            ;;
        "")
            ;;
        *)
            echo "Unknown option: \"$1\", continuing execution"
            ;;
    esac

    # Switch to next argument
    shift 1
done

# Generate and create the directory containing the simulations results
# dir_simulation_results="${dir_results}/${date}"
dir_simulation_results="${dir_results}"
mkdir -p "${dir_simulation_results}"

# Modify options and generate the database path/url, or the json path depending
# on dataset type/what main class is used
if [[ "${dataset_type}" == "refined" ]]; then
    # Prepare config file
    path_second_dataset="not_used"
    output_filename="not_used"
    db_url="${db_url_root}:${simulation_id}.db"
elif [[ "${dataset_type}" == "checkoptimal" ]]; then
    if [[ "${path_second_dataset}" == "will_be_filled" ]]; then
        error "You need to specify a second dataset with the option --secondDataset"
    fi

    output_filename="not_used"
    db_url="${db_url_root}:not_used"
else
    # Modify options
    runs=1

    # Prepare config file
    path_second_dataset="not_used"
    output_filename="${simulation_id}.json"
    db_url="${db_url_root}:not_used"
fi

# Generate the config file path and verify that it doesn't exist
path_absolute_config="${dir_simulation_results}/${simulation_id}.cfg"
if [[ -e "${path_absolute_config}" ]]; then
    error "The given simulation id has already been used, file \"${path_absolute_config}\" already exists"
fi

# Fill the config file
cat > "${path_absolute_config}" <<EOF
# Unique simulation identifier
simulationId              = ${date}_${simulation_id}

# Parameters
numRuns                   = ${runs}
numRounds                 = ${rounds}
neighbourhoodSize         = ${neighboorhood_size}
fractionHiddenItems       = ${fraction_hidden_items}

# Dataset reader
datasetType               = ${dataset_type}
inputFilename             = ${path_dataset}

# Optimal metrics checker
secondInputFilename       = ${path_second_dataset}

# Dataset refiner
minSubscribers            = ${min_subscribers}
ratingThreshold           = ${rating_threshold}

# File writer for calculations
outputFilename            = ${output_filename}

# Database writer for simulations
dbConnector               = ${db_connector}
dbUrl                     = ${db_url}
dbBatchSize               = ${db_batch_size}

# Free choice
staticAlgoForced          = ${static}
algoToStartWith           = ${start_algo}
numTimesToSwitchFalseItems= ${times_switch_false_items}

# Algorithms modifiers
withDetCurrAlgo           = ${detcurralgo}
withIncPrevRounds         = ${incprevrounds}
withIncSimNodes           = ${incsimnodes}
detCurrAlgoWeighting      = ${detcurralgo_weighting}
prevRoundsWeighting       = ${incprevrounds_weighting}
simNodesWeighting         = ${incsimnodes_weighting}
detrimentValue            = ${detriment_value}
numPrevRoundsToInc        = ${prev_rounds}
incPrevRoundsMaxWeight    = ${incprevrounds_max_weight}
roundsOfCoolOff           = ${cool_off}
metricMargin              = ${metric_margin}

# Multithreading
threadpoolSize            = ${thread_pool_size}
threadsTimeout            = ${threads_timeout}

# Debug
withDebugPrinting         = ${debug_printing}
withStaticSeed            = ${static_seed}
withMaxNodes              = ${with_max_nodes}
maxNodes                  = ${max_nodes}
EOF

path_relative_config=$(realpath --relative-to=. "${path_absolute_config}")
echo "Generated config file: ${path_relative_config}"

exit 0
