#! /bin/bash
# Contains global variables/constants for other scripts to use

# Display the error message, a generic message and exits the script
function error() {
    generic="Use the --help option to display the usage of this script"
    [[ -n "$1" ]] && echo "$1" >&2
    echo -e "${generic}" >&2
    echo "Aborting execution" >&2
    exit 1
}

# If realpath doesn't exist, redefine a simple version of it that gives the
# absolute path of the last given argument
function realpath() {
    local bin="/usr/bin/realpath"
    local dir="${@: -1}"

    type "${bin}" &> /dev/null
    if [[ $? -eq 0 ]]; then
        # Use the existing one
        "${bin}" "$@"
    else
        # Redefine it (don't handle options)
        if [[ "${dir}" =~ ^\/ ]]; then
            echo "${dir}" # Use the last argument
        else
            echo "$PWD/${dir}"
        fi
    fi
}


# Project directories as absolute path
dir_project=$(realpath "..")
dir_bin="${dir_project}/bin"
dir_figures="${dir_project}/figures"
dir_datasets="${dir_project}/datasets"
dir_results="${dir_project}/results"
dir_simulator="${dir_project}/simulator"
dir_tmp_csv="${dir_results}/tmp_csv"
dir_tmp_plot="${dir_figures}/tmp_plot"

# Scripts
script_compile="${dir_bin}/compile.sh"
script_gen_cfg="${dir_bin}/generate_cfg.sh"

# Get the date of the day in the format "1970-12-31"
date=$(date +%F)
