#! /bin/bash
# Generate all the config files needed to plot all the graphs

# Global constants and functions
cd $(dirname "$0")
. global.sh
cd "${OLDPWD}"


##### Constants
# which dataset to refine
dataset_type="movielens"
path_dataset_raw="${dir_datasets}/movielens_1k_users_raw.data"
# dataset_type="twitter"
# path_dataset_raw="${dir_datasets}/twitter_16k_raw.json"

# dataset refinement
simulation_id_refinement="dataset_refinement"
path_dataset_refined="${simulation_id_refinement}.json"

# check optimal
simulation_id_check_optimal="check_optimal"
simulation_id_refinement_2="dataset_refinement_2"
path_dataset_refined_2="${simulation_id_refinement_2}.json"
#####


common_options="$@"

# dataset refinement
"${script_gen_cfg}" "${dataset_type}" "${simulation_id_refinement}" "${path_dataset_raw}" ${common_options}

# Uncomment these 2 lines to check for differences in two refined datasets
"${script_gen_cfg}" "${dataset_type}" "${simulation_id_refinement_2}" "${path_dataset_raw}" ${common_options}
"${script_gen_cfg}" checkoptimal "${simulation_id_check_optimal}" "${path_dataset_refined}" ${common_options} --secondDataset "${path_dataset_refined_2}"

# static random
"${script_gen_cfg}" refined "static_random"   "${path_dataset_refined}" ${common_options} --static --startAlgo random
# static best algo
"${script_gen_cfg}" refined "static_bestalgo" "${path_dataset_refined}" ${common_options} --static --startAlgo bestalgo
# static big
"${script_gen_cfg}" refined "static_big"      "${path_dataset_refined}" ${common_options} --static --startAlgo big
# static jaccard
"${script_gen_cfg}" refined "static_jaccard"  "${path_dataset_refined}" ${common_options} --static --startAlgo jaccard
# static overbig
"${script_gen_cfg}" refined "static_overbig"  "${path_dataset_refined}" ${common_options} --static --startAlgo overbig
# static overlap
"${script_gen_cfg}" refined "static_overlap"  "${path_dataset_refined}" ${common_options} --static --startAlgo overlap

# similitude basic
"${script_gen_cfg}" refined "similitude_basic"                  "${path_dataset_refined}" ${common_options}
# similitude all modifiers
"${script_gen_cfg}" refined "similitude_allmodifiers"           "${path_dataset_refined}" ${common_options} --allModifiers
# similitude detcurralgo
"${script_gen_cfg}" refined "similitude_detcurralgo"            "${path_dataset_refined}" ${common_options} --detcurralgo
# similitude incsimnodes
# "${script_gen_cfg}" refined "similitude_incsimnodes"            "${path_dataset_refined}" ${common_options} --incsimnodes
# similitude incprevrounds
# "${script_gen_cfg}" refined "similitude_incprevrounds"          "${path_dataset_refined}" ${common_options} --incprevrounds
# similitude all modifiers + cool off 0
"${script_gen_cfg}" refined "similitude_allmodifiers_cooloff0"  "${path_dataset_refined}" ${common_options} --allModifiers --coolOff 0
# similitude all modifiers + cool off 5
"${script_gen_cfg}" refined "similitude_allmodifiers_cooloff5"  "${path_dataset_refined}" ${common_options} --allModifiers --coolOff 5

# similitude incsimnodes or incprevrounds with weighting
for weight in 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0; do
	# similitude incsimnodes
	"${script_gen_cfg}" refined "similitude_incsimnodes_weight${weight}"   "${path_dataset_refined}" ${common_options} --incsimnodes --incsimnodesWeighting "${weight}"
	# similitude incprevrounds
	"${script_gen_cfg}" refined "similitude_incprevrounds_weight${weight}" "${path_dataset_refined}" ${common_options} --incprevrounds --incprevroundsWeighting "${weight}"
done

exit 0
