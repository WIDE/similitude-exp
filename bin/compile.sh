#! /bin/bash
# Compiles the simulator

# Global constants and functions
cd $(dirname "$0")
. global.sh
cd "${OLDPWD}"


cd "${dir_simulator}"
javac -extdirs "${dir_project}"/lib/ $(find "${dir_project}"/simulator -name '*.java')
