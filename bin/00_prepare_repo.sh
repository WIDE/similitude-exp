#!/bin/bash
# Author: Pierre-Louis Roman, 2018-06-08

# Global constants and functions
cd $(dirname "$0")
. global.sh
cd "${OLDPWD}"

dir_lib="${dir_project}/lib"
mkdir -p "${dir_datasets}" "${dir_lib}" "${dir_tmp_csv}" "${dir_tmp_plot}"

# Fetch and extract the movielens dataset with 1OOk ratings and 1k users
filename="${dir_datasets}/movielens_1k_users_raw.data"
if [[ -f "${filename}" ]]; then
    echo '===== Dataset MovieLens 100k already downloaded'
else
    echo '===== Fetching the movielens 100k dataset'
    tmp=$(mktemp)
    url='http://files.grouplens.org/datasets/movielens/ml-100k.zip'
    curl "${url}" > "${tmp}"
    checksum=$(md5sum "${tmp}" | cut -d' ' -f1)
    # http://files.grouplens.org/datasets/movielens/ml-100k.zip.md5
    [[ "${checksum}" != '0e33842e24a9c977be4e0107933c0723' ]] \
        && echo 'Error on the checksum of the movielens 100k dataset. Please inspect.' &>2 \
        && exit
    unzip -j "${tmp}" ml-100k/u.data -d "${dir_datasets}/" \
        && mv -i "${dir_datasets}/u.data" "${filename}"
    [[ $? -ne 0 ]] \
        && echo 'Error extracting the ratings from the movielens 100k dataset. Please inspect' &>2 \
        && exit
    rm "${tmp}" # Clean tmp file
    unset url tmp checksum
fi
unset filename

# Fetch and extract the movielens dataset with 1M ratings and 6k users
filename="${dir_datasets}/movielens_6k_users_raw.data"
if [[ -f "${filename}" ]]; then
    echo '===== Dataset MovieLens 1M already downloaded'
else
    echo '===== Fetching the MovieLens 1M dataset'
    tmp=$(mktemp)
    url='http://files.grouplens.org/datasets/movielens/ml-1m.zip'
    curl "${url}" > "${tmp}"
    checksum=$(md5sum "${tmp}" | cut -d' ' -f1)
    # http://files.grouplens.org/datasets/movielens/ml-1m.zip.md5
    [[ "${checksum}" != 'c4d9eecfca2ab87c1945afe126590906' ]] \
        && echo 'Error on the checksum of the movielens 1M dataset. Please inspect.' &>2 \
        && exit
    unzip -j "${tmp}" ml-1m/ratings.dat -d "${dir_datasets}/" \
        && mv -i "${dir_datasets}/ratings.dat" "${filename}"
    [[ $? -ne 0 ]] \
        && echo 'Error extracting the ratings from the movielens 1M dataset. Please inspect' &>2 \
        && exit
    rm "${tmp}" # Clean tmp file
    unset url tmp checksum
fi
unset filename

#echo 'Fetching the anonymized Twitter/Foursquare dataset'
# http://ftaiani.ouvaton.org/ressources/onlyBayLocsAnonymised_21_Oct_2011.tgz
# tmp=$(mktemp)
# url='http://ftaiani.ouvaton.org/ressources/onlyBayLocsAnonymised_21_Oct_2011.tgz'
# curl "${url}" > "${tmp}"
# checksum=$(sha1sum "${tmp}" | cut -d' ' -f1)
# [[ "${checksum}" != '56467f54fc04c280fc0ad6c04043e57d9b264369' ]] \
#     && echo 'Error on the checksum of the Twitter/Foursquare dataset. Please inspect.' &>2 \
#     && exit

# Fetch the sqlite jdbc library
filename="${dir_lib}/sqlite-jdbc-3.8.6.jar"
if [[ -f "${filename}" ]]; then
    echo '===== Library sqlite jdbc already downloaded'
else
    echo '===== Fetching the sqlite jdbc jar'
    url='http://central.maven.org/maven2/org/xerial/sqlite-jdbc/3.8.6/sqlite-jdbc-3.8.6.jar'
    curl "${url}" > "${filename}"
    checksum=$(sha1sum "${filename}" | cut -d' ' -f1)
    # http://central.maven.org/maven2/org/xerial/sqlite-jdbc/3.8.6/sqlite-jdbc-3.8.6.jar.sha1
    [[ "${checksum}" != '16ea8dd96d82a90c386c60f95c7da5813f65ac21' ]] \
        && echo 'Error on the checksum of the sqlite jdbc jar. Please inspect.' &>2 \
        && exit
    unset url checksum
fi
unset filename

# Fetch the json simple library
filename="${dir_lib}/json-simple-1.1.1.jar"
if [[ -f "${filename}" ]]; then
    echo '===== Library json simple already downloaded'
else
    echo '===== Fetching the json simple jar'
    url='http://central.maven.org/maven2/com/googlecode/json-simple/json-simple/1.1.1/json-simple-1.1.1.jar'
    curl "${url}" > "${filename}"
    checksum=$(sha1sum "${filename}" | cut -d' ' -f1)
    # http://central.maven.org/maven2/com/googlecode/json-simple/json-simple/1.1.1/json-simple-1.1.1.jar.sha1
    [[ "${checksum}" != 'c9ad4a0850ab676c5c64461a05ca524cdfff59f1' ]] \
        && echo 'Error on the checksum of the json simple jar. Please inspect.' &>2 \
        && exit
    unset url checksum
fi
unset filename
