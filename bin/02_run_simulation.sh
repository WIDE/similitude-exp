#! /bin/bash
# Run the simulation with the given config file

# Global constants and functions
cd $(dirname "$0")
. global.sh
cd "${OLDPWD}"


##### Script options
compile=false
foreground=false
#####


function usage() {
	message="\
Usage: $(basename $0) <config file> [options...]
Run the simulation with the given config file and logs the output

Parameters:
    <config file>
        path of the config file

Script options:
    -h, --help
        display this message
    -c, --compile
        compile the simulator
    -f, --foreground
        run the job in foreground (while still logging the output)\
"

	echo "${message}"
	exit 0
}


# First check if only the help is asked
case "$1" in
    -h|--help|help|"") usage;;
esac

# Assign the firsts arguments
path_config_file="$1"

# Verify that the config file exists and is a regular file/link
if [[ ! -f "${path_config_file}" ]] && [[ ! -L "${path_config_file}" ]]; then
    error "The given config file needs to be an existing regular file or a link: ${path_config_file}"
fi

# Parse the options
shift 1 # Remove the first argument from the array
while [[ "$#" != 0 ]] ; do
    case "$1" in
        # Script options
        -c|--compile) compile=true;;
        -f|--foreground) foreground=true;;
        -h|--help)
            usage
            ;;
        "")
            ;;
        *)
            echo "Unknown option: \"$1\", continuing execution"
            ;;
    esac

    # Switch to next argument
	shift 1
done

# Compile the simulator if asked
if [[ "${compile}" == "true" ]] ; then
    echo "----- Begin simulator compiling"
    "${script_compile}"
    echo "----- End of simulator compiling"
    echo
fi

# Add the simulator directory to the classpath
export CLASSPATH="${CLASSPATH}:${dir_simulator}"

# Add the necessary libraries to the classpath
for jar in "${dir_project}"/lib/*.jar; do
    echo "${CLASSPATH}" | grep -q "${jar}"
    [[ $? -ne 0 ]] && export CLASSPATH="${CLASSPATH}:${jar}"
done

# Change directory to the one containing the config file
cd $(dirname "${path_config_file}")

# Compute the log file path (= config file path - .cfg + .log)
name_config_file=$(basename "${path_config_file}")
name_log_file=$(basename "${path_config_file}" .cfg).log
path_log_file=$(dirname "${path_config_file}")/"${name_log_file}"

# Run the simulator and display the log file path
if [[ "${foreground}" == "true" ]] ; then
    echo "About to start a job, resulting log file: ${path_log_file}"
    echo "=========="
    { time java main.Main "${name_config_file}"; } |& tee -a "${name_log_file}"
else
    { time java main.Main "${name_config_file}"; } &>> "${name_log_file}" &
    echo "Started a job, resulting log file: ${path_log_file}"
fi

exit 0
