#! /usr/bin/env Rscript

# Output in pdf file
pdf(file = "distribution_optimal.pdf",
	width = 6,
	height = 4,
	)

# Fill the data matrix
data <- as.matrix(read.table(text =
	"            Big      FrereJacc  OverBig  Overlap
twitter_5k       15.76    75.31      15.85    50.49
movielens_6k     24.20    81.40      24.25    36.43
", header = TRUE, sep = ""))

# Print the data (debug)
# data

# Barplot with horizontal bars, limit to 100
bplt <- barplot(
	data,
	horiz = TRUE,
	beside = TRUE,
	las = 1,
	xlim = c(0,100),
	xlab = "Percentage",
	legend.text = c("Twitter", "MovieLens"),
	cex.axis = 1.2,
	cex.names = 1.0,
	)

# Add the values at the end of each bar
text(
	x = data + 7,
	y = bplt,
	labels = as.character(data),
	xpd = TRUE,
	cex = 1.2
	)
