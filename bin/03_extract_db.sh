#! /bin/bash
# Extract the databases

# TODO: handle databases containing multiple simulations
# get the simu_id from the config table and loop over it

# Global constants and functions
cd $(dirname "$0")
. global.sh
cd "${OLDPWD}"


function usage() {
	message="\
Usage: $(basename $0) <databases...> [options...]
Extract relevant informations from the given databases, including the ones
needed to plot the graphs

Parameters:
    <databases>
        list of simulation resulting databases

Script options:
    -h, --help
        display this message\
"

	echo "${message}"
	exit 0
}


# First check if only the help is asked
case "$1" in
    -h|--help|help|"") usage;;
esac

# Parse the arguments (databases and/or options)
while [[ $# != 0 ]] ; do
    case "$1" in
        # Script options
        -h|--help)
            usage
            ;;
        "")
			continue
            ;;

        # Assuming it's a database
        *)
			# Compute the csv file path
			path_database="$1"
			path_csv_file=$(dirname "${path_database}")/$(basename "${path_database}" .db).csv

			# Perform the extraction
			echo -e ".headers on\n.separator ,\nSELECT * FROM exp_avgs;" | sqlite3 "${path_database}" > "${path_csv_file}"

            echo "Extracted data to: ${path_csv_file}"
            ;;
    esac

    # Switch to next argument
    shift 1
done

exit 0
