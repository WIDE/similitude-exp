#!/usr/bin/ruby
# Create the .plot and .pdf of all the graphs in DIR_TMP_PLOT
# based on the .csv in the DIR_TMP_CSV

# Converting command line args to hash
args = Hash[*ARGV.map{ |arg|
              s = arg.split("=")
              [s[0].to_sym,s[1]]
            }.flatten]

# Change to the bin directory to find the rest of the paths
Dir.chdir(File.dirname(__FILE__))
dir_project  = `realpath ..`.chomp
DIR_FIGURES  = "#{dir_project}/figures"
DIR_RESULTS  = "#{dir_project}/results"
DIR_TMP_CSV  = "#{DIR_RESULTS}/tmp_csv"
DIR_TMP_PLOT = "#{DIR_FIGURES}/tmp_plot"

$header="
set terminal pdf linewidth 5 dashed size 6,4 font ',18'
set datafile separator \",\"
cd '#{DIR_TMP_CSV}'
set pointsize 0.9
set border linewidth 0.3
set grid linetype 1 linewidth 0.6 linecolor rgb \"#bbbbbb\"
"

# Map file name -> title
$default_titles = {
  1  => "All modifiers",
  2  => "Similitude (basic)",
  3  => "HeterOpt",
  5  => "detCurrAlgo",
  7  => "incPrevRounds",
  8  => "incSimNodes",
  9  => "HeterRand",
  10 => "Big",
  11 => "FrereJacc",
  12 => "OverBig",
  13 => "Overlap",
  18 => "All modifiers - 0 cool-off",
  19 => "All modifiers - 5 cool-off",
  20 => "incPrevRounds", # weightings
  21 => "incSimNodes" # weightings
}

# Define colors and point types
$colors    = ["#0266C8", "#B26500", "#F90101", "#01A901", "#3B0093", "#0101F9", "#933B00", "#000000"]
$colors_v2 = ["#F90101", "#F90101", "#F90101", "#F90101", "#0266C8", "#0266C8", "#000000"]
$pointtype = [1, 2, 4, 6, 8, 12, 10, 14]

class << $colors
  attr_accessor :color
  def next()
    @color+=1
    "linecolor rgb '#{self[@color-1]}' pointtype #{$pointtype[@color-1]}"
  end
end

class << $colors_v2
  attr_accessor :color
  def next()
    @color+=1
    "linecolor rgb '#{self[@color-1]}' pointtype #{$pointtype[@color-1]}"
  end
end

# Common code to both other functions, parse the args and ouputs common headers
def function_common(user_args)
  # default values for args
  args = {
    :output => "graph",
    :xlim => "[:]",
    :ylim => "[:]",
    :data_file => [1],
    :xlabel => "Rounds of clustering",
    :ylabel => "",
    :titles => {},
    :color_start => 0,
    :y_column => 0,
    :y_format => "",
    :key_place => "right bottom",
    :colors => $colors,
    :y2label => "",
    :y2_format => "",
  }
  user_args.each { |k,v| args[k]=v } # overwriting default args

  File.open("#{DIR_TMP_PLOT}/#{args[:output]}.plot","w") do |out|
    out.puts $header
    out.puts "set format y '#{args[:y_format]}'"
    out.puts "set xlabel '#{args[:xlabel]}'"
    out.puts "set ylabel '#{args[:ylabel]}'"
    out.puts "set key #{args[:key_place]}"
    out.puts "set output '#{DIR_TMP_PLOT}/#{args[:output]}.pdf'"
  end

  return args
end

# Called to create almost all the plots
def produce_plot(user_args)
  args = function_common(user_args)

  titles = $default_titles.clone
  args[:titles].each { |k,v| titles[k]=v }
  args[:colors].color = args[:color_start]
  format = "every ::1 with linespoints pointinterval 5"

  File.open("#{DIR_TMP_PLOT}/#{args[:output]}.plot","a") do |out|
      out.puts "plot #{args[:xlim]} #{args[:ylim]} \\"

      args[:data_file].map {|i|
          out.puts "\t'#{i}.csv' using 2:#{args[:y_column]} #{format} #{args[:colors].next()}" +
            " title '#{titles[i]}', \\"
      }
  end

  system "gnuplot < #{DIR_TMP_PLOT}/#{args[:output]}.plot && echo Plot ready: #{args[:output]}.pdf"
end

# Called to create the weightings plot
def produce_plot_weightings(user_args)
  args = function_common(user_args)

  titles = $default_titles.clone
  args[:titles].each { |k,v| titles[k]=v }
  args[:colors].color = args[:color_start]
  format = "every ::1 with linespoints pointinterval 1"

  File.open("#{DIR_TMP_PLOT}/#{args[:output]}.plot","a") do |out|
      out.puts "set ytics nomirror"
      out.puts "set y2tics"
      out.puts "set y2label '#{args[:y2label]}'"
      out.puts "set format y2 '#{args[:y2_format]}'"
      out.puts "plot #{args[:xlim]} #{args[:ylim]} \\"

      args[:data_file].map {|i|
          out.puts "\t'#{i}.csv' using 2:4 #{format} #{args[:colors].next()}" +
              " axis x1y1 title 'precision - #{titles[i]}', \\"
          out.puts "\t'#{i}.csv' using 2:3 #{format} #{args[:colors].next()}" +
              " axis x1y2 title 'recall - #{titles[i]}', \\"
      }
  end

  system "gnuplot < #{DIR_TMP_PLOT}/#{args[:output]}.plot && echo Plot ready: #{args[:output]}.pdf"
end


# Create the temporary directory if needed
`mkdir -p #{DIR_TMP_PLOT}`

precision = {
  :ylabel => "Precision",
  :y_column => 4,
  :y_format => "%.3f",
}

recall = {
  :ylabel => "Recall",
  :y_column => 3,
  :y_format => "%.2f",
}

user_switching = {
  :ylabel => "Percentage of nodes switching metrics",
  :y_column => 5,
  :y_format => "%.0f",
  :key_place => "top right",
}

user_on_target = {
  :ylabel => "Percentage of nodes on optimal metrics",
  :y_column => 6,
  :y_format => "%.0f",
}

cool_off = {
  :titles => { 1 => "All modifiers - 2 cool-off" },
}


# produce_plot({ :output => "g2", :ylim => "[0:0.5]", :data_file => [11,13,9,10,12],
#               :color_start => 1, :titles => {11 => "Jaccard" } })

# produce_plot({ :output => "g3", :data_file => [3,11,2,9]})

# produce_plot({ :output => "g4", :ylim => "[0.15:]", :data_file => [3,11,1,7,8,5]})

# produce_plot({ :output => "g5", :data_file => [3,11,1,7,8,5],
#               :y_column => 3 , :ylabel => "Precision", :y_format => "%.3f" })

# produce_plot({ :output => "g8", :ylim => "[0.15:]", :data_file => [3,11,18,19,7]})

# produce_plot({ :output => "g9", :data_file => [3,11,18,19,7],
#               :y_column => 3 , :ylabel => "Precision", :y_format => "%.3f" })

produce_plot({ :output => "modifiers_precision",
               :data_file => [2,7,8,5,1,9],
               # :ylim => "[0.011:]", # For movielens 6k
               :ylim => "[0.015:]", # For twitter 5k
               }
               .merge(precision)
               )

produce_plot({ :output => "modifiers_recall",
               :data_file => [2,7,8,5,1,9],
               :ylim => "[0.06:]",
               }
               .merge(recall)
               )

produce_plot({ :output => "modifiers_users_on_optimal",
               :data_file => [2,7,8,5,1],
               }
               .merge(user_on_target)
               )

produce_plot({ :output => "modifiers_users_switching",
               :data_file => [2,7,8,5,1],
               }
               .merge(user_switching)
               )

produce_plot({ :output => "cooloff_precision",
               :data_file => [18,1,19],
               }
               .merge(precision)
               .merge(cool_off)
               )

produce_plot({ :output => "cooloff_recall",
               :data_file => [18,1,19],
               }
               .merge(recall)
               .merge(cool_off)
               )

produce_plot({ :output => "cooloff_users_on_optimal",
               :data_file => [18,1,19],
               }
               .merge(user_on_target)
               .merge(cool_off)
               )

produce_plot({ :output => "cooloff_users_switching",
               :data_file => [18,1,19],
               }
               .merge(user_switching)
               .merge(cool_off)
               )

produce_plot({ :output => "similitude_vs_static_precision",
               :data_file => [11,13,10,12,3,9,18],
               # :key_place => "at 100,0.032 maxrows 4", # For movielens 6k
               :key_place => "at 100,0.027 maxrows 4", # For twitter 5k
               :titles => {18 => "Similitude (optimised)"},
               :colors => $colors_v2,
               }
               .merge(precision)
               )

produce_plot_weightings({ :output => "weightings_sensibility",
                          :data_file => [20,21],
                          :xlabel => "Weighting",
                          :y2label => "Recall",
                          :y2_format => "%.2f",
                          :colors => $colors_v2,
                          :color_start => 2,
                          }
                          .merge(precision)
                          )


puts "Resulting plots are in: #{DIR_TMP_PLOT}"
