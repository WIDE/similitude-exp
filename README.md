# Similitude simulation code

### Description
This repository contains the code and the helper scripts for the simulations used as evaluation in the [Similitude paper](https://hal.inria.fr/hal-01138365).

The contained code is a simulator of a decentralised social network, built for the
evaluation of P2P social recommendation concepts. The simulator takes as input a set of users accompanied with their interests, and runs a decentralised social recommendation service, outputting recall and precision metrics.

Main authors of the code: Christopher Maddock, Pierre-Louis Roman.

### Installation
The simulator requires the [json simple](https://mvnrepository.com/artifact/com.googlecode.json-simple/json-simple) and the [sqlite jdbc](https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc) libraries to work.

The datasets used for the experiments are [MovieLens 1M ratings](https://grouplens.org/datasets/movielens/1m/) and the raw Twitter/Foursquare dataset obtained via [previous work](http://ftaiani.ouvaton.org/5-ressources/index.html) from the authors. An anonymized version of this dataset is [available online](http://ftaiani.ouvaton.org/ressources/onlyBayLocsAnonymised_21_Oct_2011.tgz).

The libraries as well as the datasets can be downloaded and extracted by the script `bin/00_prepare_repo.sh`.

One helper script requires ruby to be installed, please follow its [installation guide](https://www.ruby-lang.org/en/documentation/installation/) for your environment.

**Warning**: the code is designed to work for the raw version of the Twitter/Foursquare dataset, which is not publicly available. Reproducibility of the experiments is therefore limited to the MovieLens datasets. Additional dataset readers (e.g., for the anonymized version of the Twitter/Foursquare dataset) can be implemented.

### Execution
Prepare the repository by downloading the libraries and datasets, and compiling the sources:
```
bin/00_prepare_repo.sh
bin/compile.sh
```

Prepare the simulations by generating all the configuration files after choosing a dataset between MovieLens and TwitterFoursquare:
```
edit bin/01_generate_all_cfg.sh    # Select the dataset
bin/01_generate_all_cfg.sh --runs 10 --rounds 100 --threadPoolSize 2 [optional arguments from bin/generate_cfg.sh]
```

Refine the dataset to get a new dataset containing the optimal metrics for each user:
```
bin/02_run_simulation.sh results/dataset_refinement.cfg
tail -f results/*.log    # To know when the run is over
```

Optionally, to get Figure 7 and the key numbers used in Section 5.1 of the paper, refine the dataset a second time to compare the refinements and the two optimal metric distributions:
```
bin/02_run_simulation.sh results/dataset_refinement_2.cfg
bin/02_run_simulation.sh results/check_optimal.cfg
tail -f results/*.log    # To know when the runs are over
```

Once the first refined dataset is computed, run all the simulations:
```
for cfg in results/{static,similitude}_*.cfg; do bin/02_run_simulation.sh "${cfg}" --foreground; done
tail -f results/*.log    # To know when the runs are over
```

**Warning**: running all the simulations with the given number of runs and rounds will take a lot of time to complete. You can reduce these two parameters for faster results (e.g., 2 runs and 25 rounds).

Extract information from the resulting databases to generate csv files for the plots:
```
bin/03_extract_db.sh results/*.db
```

Create the graphs from the resulting csv files:
````
bin/04_copy_csv.sh results/
bin/05_create_graph.rb
````

#### Debugging
Debug CalcMain (refinement process):
```bash
cd simulator
../bin/02* debug_calc.cfg --compile --foreground
```

Debug SimuMain (simulation process):
```bash
tar xzf results/2015-01-03.tgz --directory=results
cd simulator
../bin/02* debug_simu.cfg --compile --foreground
```

### Files
* `bin/` contains the helper scripts written in bash and ruby that can compile the code, run the simulator, aggregate the results and generate the plots
* `lib/` contains the jar used by the simulator
* `results/` contains the raw data obtained from the simulator
* `figures/` contains all the figures obtained from the results
* `simulator/` contains the code for the simulator

### Figures
A copy of the plots used for the published paper is stored in this repository:

* Fig 07: obtained by running `calculations/CheckOptimalMain` and manually inserting the numbers in `bin/graph_distribution_optimal.r`
* Fig 08: `figures/2015-02-17_twitter_5k/cooloff_precision.pdf`
* Fig 09: `figures/2015-02-17_twitter_5k/cooloff_recall.pdf`
* Fig 10: `figures/2015-02-17_twitter_5k/modifiers_users_on_optimal.pdf`
* Fig 11: `figures/2015-02-17_twitter_5k/modifiers_users_switching.pdf`
* Fig 12: `figures/2015-02-17_twitter_5k/weightings_sensibility.pdf`
* Fig 13: `figures/2015-02-17_twitter_5k/cooloff_precision.pdf`
* Fig 14: `figures/2015-02-17_twitter_5k/cooloff_recall.pdf`
* Fig 15: `figures/2015-02-17_movielens_6k/modifiers_precision.pdf`
* Fig 16: `figures/2015-02-17_movielens_6k/modifiers_recall.pdf`
* Fig 17: `figures/2015-02-17_twitter_5k/similitude_vs_static_precision.pdf`
* Fig 18: `figures/2015-02-17_movielens_6k/similitude_vs_static_precision.pdf`

### Database architecture
Each simulation produces a database containing all the output.

Each simulation has a unique identifier used for potential database merges.

Each database contains the following three tables:

* `end_stats` contains the statistics computed at the end of the experiment
* `exp_avgs` contains the statistics computed each round (precision, recall, ...)
* `users_algo_history` contains the history of each user similarity metric

### Code architecture
The code is separated in five packages:

* `algorithms` contains the different similarity metrics
* `calculations` contains the code for the dataset refinement part
* `io` contains the dataset readers/writers and the database writer
* `main` contains the core of the code on the decentralized social network and recommendation system
* `util` contains miscellaneous code

There are three java main files:

* `calculations/CalcMain` refines a dataset (see details below)
* `calculations/CheckOptimalMain` checks the differences between two refined datasets (used for Figure 7 and Section 5.1 of the paper)
* `main/SimuMain` runs an experiment with a given static similarity metric or one of the variants of Similitude

### Dataset refinement
The dataset refinement process is composed of the following steps:

* Remove the least popular items from the user profiles such that each item must be in at least X user profiles (X set in config file)
* Remove users who have less than 2 items in their profile
* Remove users who have less than 5 items in their visible profile (training set composed of 80% of their profile) and less than 2 items in their hidden profile (validation set composed of 20% of their profile)
* Perform one run per similarity metric, where all users are statically assigned the same metric to determine the distribution of optimal metrics (Figure 7 in the paper)
* Write the refined dataset to disk

### Development tips
* See the debug variables at the bottom of the configuration files
* To see if you introduced errors while working on `calculations/CalcMain.java`, you can print the number of users who have 1/2/3/4 best metrics and the number of users using each metric as these numbers should remain identical between runs with a static seed.
* Similar comment when working on `main/SimuMain.java`, you can print the number of users on a best metric and the number of users using each metric.
